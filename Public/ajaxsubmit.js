/*
    * 函数说明 参数格式
    * btntype："#submit"
    * url:"<?php echo U(member/login);?>"
    * type:post|get
    * dataType:json,xml等
    * formtype:"#fomrtype"
    * */
function ajaxsubmit(btntype,url,type,dataType,formtype) {

    $(btntype).click(function () {
        var options = {
            url: url, //form提交数据的地址
            type: type, //form提交的方式(method:post/get)
            //target:target, //服务器返回的响应数据显示在元素(Id)号确定
            //beforeSubmit:function(), //提交前执行的回调函数

            success: function (data) {
                alert(data.info);
                location.href = data.url;

            }, //提交成功后执行的回调函数
            dataType: dataType, //服务器返回数据类型
             clearForm:false, //提交成功后是否清空表单中的字段值
           restForm:false, //提交成功后是否重置表单中的字段值，即恢复到页面加载时的状态
            timeout: 3000 //设置请求时间，超过该时间后，自动退出请求，单位(毫秒)。
        };
        $(formtype).ajaxForm(options);
    });
}

/*
*函数说明：
* 此函数用于删除操作中的ajaxget提交
* 参数 this;
*
 * */
function  deldata(obj) {
    if (confirm('确认删除吗？'))
    {
        var url=$(obj).attr("url");
        $.ajax({
            type: "get",
            url: url,
            success: function (data) {
                alert(data.info);
                location.href = data.url;
            }
        });

    }
}

