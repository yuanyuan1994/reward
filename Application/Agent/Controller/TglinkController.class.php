<?php

namespace Agent\Controller;

use Think\Controller;

class TglinkController extends CommonController
{
    public function add()
    {

        $spmodel = D('Shipin');
        if (IS_POST) {
            $cfmodel = D('Config');
            $cfdata = $cfmodel->field('zym,dwz')->find();
            $data = $_POST['data'];
            $tgmode = M('Tglink');

            $t = $spmodel->field('id,name,zykey,url')->where(['id' => ['in', $data]])->select();
            $arr = [];
            $arr1 = [];
            $re = M('agent')->field('domain')->where(['id' => $_SESSION['Agent_id']])->find();
            $arr2 = explode(',', $re['domain']);

            foreach ($t as $v) {
                $i = rand(0, count($arr2) - 1);
                $ddh = substr(md5($v['zykey']), 0, 10);
                $url = $arr2[$i] . 'index.php/Home/Index/index?code=' . $v['zykey'] . $ddh;
                $longurl = urlencode($url);
                $arr['name'] = $v['name'];
                $arr['code'] = $v['zykey'] . $ddh;
                $arr['shortlink'] = dwzlink($longurl, $cfdata['dwz']);
                $arr['userid'] = $_SESSION['Agent_id'];
                if (!$tgmode->where(array('shortlink' => $arr['shortlink']))->find()) {
                    $tgmode->add($arr);
                }
                array_push($arr1, $arr);
            }
            $arr1 = json_encode($arr1);
            echo $arr1;
        } else {
            $spdata = $spmodel->field('id,name,zykey,url')->where(['userid' => $_SESSION['Agent_id']])->select();
            // 设置页面中的信息
            $this->assign(array(
                'spdata' => $spdata,
            ));
            $this->display();
        }

    }

    public function edit()
    {
        $id = I('get.id');
        if (IS_POST) {
            $model = D('Tglink');
            if ($model->create(I('post.'), 2)) {
                if ($model->save() !== FALSE) {
                    $this->success('修改成功！', U('lst', array('p' => I('get.p', 1))));
                    exit;
                }
            }
            $this->error($model->getError());
        }
        $model = M('Tglink');
        $data = $model->find($id);
        $this->assign('data', $data);

        $this->display();
    }

    public function delete()
    {
        $model = D('Tglink');
        if ($model->delete(I('get.id', 0)) !== FALSE) {
            $this->success('删除成功！', U('lst', array('p' => I('get.p', 1))));
            exit;
        } else {
            $this->error($model->getError());
        }
    }

    public function lst()
    {
        $model = D('Tglink');
        $data = $model->search();
        $this->assign(array(
            'data' => $data['data'],
            'page' => $data['page'],
        ));

        $this->display();
    }
}