<?php

namespace Agent\Controller;

use Think\Controller;

class ShipinController extends CommonController
{
    public function add()
    {
        if (IS_POST) {
            $model = D('Shipin');
            if ($model->create(I('post.'), 1)) {
                if ($id = $model->add()) {
                    $this->success('添加成功！', U('dllst?p=' . I('get.p')));
                    exit;
                }
            }
            $this->error($model->getError());
        }
        $sptype = M('sptype');
        $data = $sptype->select();

        // 设置页面中的信息
        $this->assign(array(
            'data' => $data,
        ));
        $this->display();
    }

    public function edit()
    {
        $id = I('get.id');

        if (IS_POST) {
            $model = D('Shipin');
            if ($model->create(I('post.'), 2)) {
                if ($model->save() !== FALSE) {
                    $this->success('修改成功！', U('dllst', array('p' => I('get.p', 1))));
                    exit;
                }
            }
            $this->error($model->getError());
        }
        $model = M('Shipin');
        $data = $model->find($id);
        $sptype = M('sptype');
        $data1 = $sptype->select();
        $this->assign(array('data' => $data, 'sptypedata' => $data1));


        $this->display();
    }

    public function delete()
    {
        $model = D('Shipin');
        if ($model->delete(I('get.id', 0)) !== FALSE) {
            $this->success('删除成功！', U('dllst', array('p' => I('get.p', 1))));
            exit;
        } else {
            $this->error($model->getError());
        }
    }

    public function lst()
    {
        $model = D('Shipin');
        $data = $model->search();

        $this->assign(array(
            'data' => $data['data'],
            'page' => $data['page'],
        ));


        $this->display();
    }

    public function dllst()
    {
        $model = D('Shipin');
        $data = $model->dllst();
        $sptype = M('sptype');
        $data1 = $sptype->select();
        $this->assign(array(
            'data' => $data['data'],
            'page' => $data['page'],
            'sptypedata' => $data1,
        ));
        $this->display();
    }

    /**
     *将公共视频发布为私有视频
     */
    public function add_dlsp()
    {
        $id = I('get.id');
        $sptype = I('get.sptype');
        $spmodel = M('shipin');
        $res = $spmodel->where(['id' => $id])->find();
        /*if($res = M('shipin')->where(['url'=>$res['url']])->find()){
            $this->error('该视频已发布过，请前往观看...',U('dllst?p='.I('get.p')));
        }*/
        $data['money'] = $res['money'];
        $data['sj'] = $res['sj'];
        $data['url'] = $res['url'];
        $data['name'] = $res['name'];
        $data['zykey'] = $res['zykey'];
        $data['allow'] = $res['allow'];
        $data['userid'] = $_SESSION['Agent_id'];
        $data['sptype'] = $sptype;
        $data['time'] = time();
        $data['bigpic'] = $res['bigpic'];
        $data['smallpic'] = $res['smallpic'];

        $jc = $spmodel->where(['userid' => session('Agent_id'), 'zykey' => $res['zykey']])->find();
        if ($jc) {
            $this->success('视频已存在！', U('dllst'), true);
            exit();
        } elseif ($result = M('shipin')->add($data)) {
            $this->success('发布成功！', U('dllst'), true);
            exit;
        }
    }

}