<?php
namespace Agent\Controller;
use Think\Controller;
class TxorderController extends CommonController
{
    public function add()
    {
    	if(IS_POST)
    	{
    	    $username = I('post.username');
            $password = I('post.password');
            $txmoney = I('post.txmoney');

            $data = M('agent')->field('money,password,txfl')->where(['id' =>$_SESSION['Agent_id']])->find();

            if($txmoney>$data['money']){
                $this->error('可提金额不足！！！');
            }
            if(md5($password)!=$data['password']){
                $this->error('提现密码不正确！！！');
            }
            $array['ddh']    =time().rand(1000,9999);
            $array['userid'] =$_SESSION['uid'];
            $array['time']   =time();
            $array['name']   =$username;
            $array['money']  =$txmoney;
            $array['txfl']   =$data['txfl'];
            if($re = M('txorder')->add($array)){

                $result = M('agent')->where(['id' =>$_SESSION['Agent_id']])->save(['money'=>$data['money']-$txmoney]);

                if($result){
                    $this->success('提现申请成功！！！',U('lst'),true);
                }
            }

    	}else{

            $data = M('agent')->field('money')->where(['id' =>$_SESSION['Agent_id']])->find();

            // 设置页面中的信息
            $this->assign(array(
                '_page_title' => '添加',
                '_page_btn_name' => '列表',
                'data' => $data,

            ));
            $this->display();
        }

    }

    public function lst()
    {
    	$model = D('Txorder');
    	$data = $model->search();
    	$this->assign(array(
    		'data' => $data['data'],
    		'page' => $data['page'],
    	));

		// 设置页面中的信息
		$this->assign(array(
			'_page_title' => '列表',
			'_page_btn_name' => '添加',
			'_page_btn_link' => U('add'),
		));
    	$this->display();
    }
    public function edit()
    {
        $id = I('get.id');
        $model = M('Txorder');
        $data = ['zt' => '4'];
        $model->where(['id' => $id])->save($data);
        if ($model->where(['id' => $id])->save($data) !== FALSE) {
            $this->success('取消成功！', U('lst', array('p' => I('get.p', 1))), true);
            exit;
        } else {
            $this->error($model->getError(), '', true);
        }
        $this->display();
    }

}