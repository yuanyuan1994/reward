<?php
namespace Agent\Model;
use Think\Model;
class TxorderModel extends Model 
{
	protected $insertFields = array('ddh','time','name','txfl','money','zt');
	protected $updateFields = array('id','ddh','time','name','txfl','money','zt');
	protected $_validate = array(
		array('ddh', 'require', '订单号不能为空！', 1, 'regex', 3),
		array('ddh', '1,255', '订单号的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('time', 'require', '申请时间不能为空！', 1, 'regex', 3),
		array('name', 'require', '提款人姓名不能为空！', 1, 'regex', 3),
		array('name', '1,30', '提款人姓名的值最长不能超过 30 个字符！', 1, 'length', 3),
		array('txfl', 'require', '提现费率不能为空！', 1, 'regex', 3),
		array('txfl', '1,255', '提现费率的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('money', 'require', '提现金额不能为空！', 1, 'regex', 3),
		array('money', '1,255', '提现金额的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('zt', '1,20', '状态的值最长不能超过 20 个字符！', 2, 'length', 3),
	);
	public function search($pageSize = 20)
	{
		/**************************************** 搜索 ****************************************/
		$where = array();
		if($ddh = I('get.ddh'))
			$where['ddh'] = array('like', "%$ddh%");
		$timefrom = I('get.timefrom');
		$timeto = I('get.timeto');
		if($timefrom && $timeto)
			$where['time'] = array('between', array(strtotime("$timefrom 00:00:00"), strtotime("$timeto 23:59:59")));
		elseif($timefrom)
			$where['time'] = array('egt', strtotime("$timefrom 00:00:00"));
		elseif($timeto)
			$where['time'] = array('elt', strtotime("$timeto 23:59:59"));
		if($zt = I('get.zt'))
			$where['zt'] = array('like', "%$zt%");
		/************************************* 翻页 ****************************************/
		$count = $this->alias('a')->where($where)->count();
		$page = new \Think\Page($count, $pageSize);
		// 配置翻页的样式
		$page->setConfig('prev', '上一页');
		$page->setConfig('next', '下一页');
		$data['page'] = $page->show();
		/************************************** 取数据 ******************************************/
		$data['data'] = $this->alias('a')->where($where)->group('a.id')->limit($page->firstRow.','.$page->listRows)->select();
		return $data;
	}
	// 添加前
	protected function _before_insert(&$data, $option)
	{
	}
	// 修改前
	protected function _before_update(&$data, $option)
	{
	}
	// 删除前
	protected function _before_delete($option)
	{
		if(is_array($option['where']['id']))
		{
			$this->error = '不支持批量删除';
			return FALSE;
		}
	}
	/************************************ 其他方法 ********************************************/
}