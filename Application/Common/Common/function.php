<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/5
 * Time: 15:57
 */
function showImage($url, $width = '', $height = '')
{
    $ic = C('IMAGE_CONFIG');
    if ($width)
        $width = "width='$width'";
    if ($height)
        $height = "height='$height'";
    echo "<img $width $height src='{$ic['viewPath']}$url' />";
}

function uploadOne($imgName, $dirName, $thumb = array())
{
    // 上传LOGO
    if (isset($_FILES[$imgName]) && $_FILES[$imgName]['error'] == 0) {
        $ic = C('IMAGE_CONFIG');
        $upload = new \Think\Upload(array(
            'rootPath' => $ic['rootPath'],
            'maxSize' => $ic['maxSize'],
            'exts' => $ic['exts'],
        ));// 实例化上传类
        $upload->savePath = $dirName . '/'; // 图片二级目录的名称
        // 上传文件
        // 上传时指定一个要上传的图片的名称，否则会把表单中所有的图片都处理，之后再想其他图片时就再找不到图片了
        $info = $upload->upload(array($imgName => $_FILES[$imgName]));
        if (!$info) {
            return array(
                'ok' => 0,
                'error' => $upload->getError(),
            );
        } else {
            $ret['ok'] = 1;
            $ret['images'][0] = $logoName = $info[$imgName]['savepath'] . $info[$imgName]['savename'];//文件路径名

            // 判断是否生成缩略图
            if ($thumb) {
                $image = new \Think\Image();
                // 循环生成缩略图

                foreach ($thumb as $k => $v) {

                    $ret['images'][$k + 1] = $info[$imgName]['savepath'] . 'thumb_' . $k . '_' . $info[$imgName]['savename'];
                    // 打开要处理的图片
                    $image->open($ic['rootPath'] . $logoName);
                    $image->thumb($v[0], $v[1])->save($ic['rootPath'] . $ret['images'][$k + 1]);
                }
            }
            return $ret;
        }
    }
}

function dwzlink($url, $peizhi)
{
    if ($peizhi == 1) {

        $html = file_get_contents("http://980.so/api.php?format=json&url=" . $url);
        $result = json_decode($html, true);
        return $result[url];

    } elseif ($peizhi == 2) {

        $html = file_get_contents("http://suo.im/api.php?format=json&url=" . $url);
        $result = json_decode($html, true);
        return $result[url];

    } elseif ($peizhi == 3) {
        $html = file_get_contents("http://api.c7.gg/api.php?format=json&url=" . $url);
        $result = json_decode(trim($html, chr(239) . chr(187) . chr(191)), true);
        return $result[url];

    } elseif ($peizhi == 4) {

        $html = file_get_contents("http://api.kks.me/api.php?format=json&url=" . $url);
        $result = json_decode(trim($html, chr(239) . chr(187) . chr(191)), true);
        return $result[url];

    } elseif ($peizhi == 5) {

        $html = file_get_contents("http://api.rrd.me/api.php?format=json&url=" . $url);
        $result = json_decode(trim($html, chr(239) . chr(187) . chr(191)), true);
        return $result[url];

    } elseif ($peizhi == 6) {

        $html = file_get_contents("http://api.uee.me/api.php?format=json&url=" . $url);
        $result = json_decode(trim($html, chr(239) . chr(187) . chr(191)), true);
        return $result[url];

    } elseif ($peizhi == 7) {

        $html = file_get_contents("http://6du.in/?is_api=1&lurl=" . $url);
        return $html;

    } elseif ($peizhi == 8) {

        $html = file_get_contents("http://mrw.so/api.php?format=json&url=" . $url);
        $result = json_decode(trim($html, chr(239) . chr(187) . chr(191)), true);
        return $result[url];

    } elseif ($peizhi == 9) {

        $html = file_get_contents("http://api.u6.gg/api.php?url=" . $url);
        $result = substr($html, 9);
        return $result;

    } elseif ($peizhi == 10) {

        $url2 = file_get_contents("http://api.t.sina.com.cn/short_url/shorten.json?source=3271760578&url_long=" . $url);
        $json = json_decode($url2);
        $zl = $json[0]->url_short;;
        return $zl;

    }
}
