<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="format-detection" content="telephone=no"/>
    <meta content="email=no" name="format-detection"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>首页</title>
    <link rel="stylesheet" href="/Public/Home/css/public.css">
</head>

<body>
<!-- 头部 -->
<div class="header flex">
    <a href="" class="logo">
        <img src="/Public/Home/img/logo.png" alt="">
    </a>
    <form action="">
        <input type="text">
        <input type="submit" value="">
    </form>
    <a href="<?php echo U('info');?>" class="user">
        <img src="/Public/Home/img/hyzx.png" alt="">
    </a>
</div>
<!-- 导航条 -->
<div class="nav flex">
    <div>
        <a href="" class="select">首页</a>
    </div>
    <div>
        <a href="">视频</a>
    </div>
    <div>
        <a href="">直播</a>
    </div>
    <div>
        <a href="">登录</a>
    </div>
</div>
<!-- 横幅 -->
<?php if(is_array($ggdata)): foreach($ggdata as $key=>$v): ?><div class="banner">
        <a href="<?php echo ($v['url']); ?>" target="_blank"><?php echo showimage($v['image'],'100%',50);?></a>
    </div><?php endforeach; endif; ?>
<!-- 中部内容 -->
<div class="main">
    <?php if(is_array($data)): foreach($data as $k=>$v): ?><ul class="theater clear">
            <?php if(!empty($v)):?>
            <h2 class="theater_title">
                <img src="/Public/Home/img/dsj.png" alt=""><?php echo ($k); ?></h2>
            <?php if(is_array($v)): foreach($v as $key=>$v2): ?><li class="theater_items">
                    <a href="<?php echo U('watch?code='.$v2['zykey'].substr(md5(rand(0,200)),0,15));?>">
                        <?php showImage($v2['smallpic'],50,50);?>
                        <strong><?php echo ($v2['name']); ?></strong>
                    </a>
                </li><?php endforeach; endif; ?>
            <?php endif;?>
        </ul><?php endforeach; endif; ?>
</div>
</body>

</html>
<script src="/Public/Home/js/jquery-1.12.4.min.js"></script>
<script src="/Public/Home/js/common.js"></script>