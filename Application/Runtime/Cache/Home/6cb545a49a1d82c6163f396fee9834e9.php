<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="format-detection" content="telephone=no"/>
    <meta content="email=no" name="format-detection"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>首页</title>
    <link rel="stylesheet" href="/Public/Home/css/public.css">
</head>
<body>
<div class="dz_head">
    <img src="/Public/Home/img/fh.png" onclick="javascript :history.back(-1)">
    <span>充值会员</span>
</div>
<form class="way" id="myform">
    <label for="" class="line ">
        请选择充值时间：
    </label>
    <?php if(is_array($data)): foreach($data as $key=>$v): ?><label for="" class="line">
            <img src="/Public/Home/img/yue.png">
            <ul>
                <li class="x-title"><?php echo ($v['name']); ?>：<span><?php echo ($v['money']); ?>元</span>
                    <input type="radio" name="amount" value="<?php echo ($v['money']); ?>">
                </li>
            </ul>
        </label><?php endforeach; endif; ?>

    <label for="" class="line ">
        请选择支付方式：
    </label>
    
    <label for="" class="line">
        <img src="/Public/Home/img/icon-zhifubao.png">
        <ul>
            <li class="x-title">支付宝支付
                <input type="radio" checked name="payer" value="alipay">
            </li>
        </ul>
        <input type="hidden" name="module" value="2">
        <input type="hidden" name="username" value="<?php echo session('member_name');?>">
        <input type="hidden" name="uid" value="<?php echo session('uid');?>">
    </label>
    <label for="" class="line">
        <input type="button" id="submit" value="立即充值" style="margin-bottom: 0;">
    </label>
</form>
</body>

</html>
<script src="/Public/Home/js/jquery-1.12.4.min.js"></script>
<script src="/Public/Home/js/common.js"></script>
<script>
    $("#submit").click(function () {

        var data = $("#myform").serialize();
        console.log(data);

        $.ajax({

            type: 'post',
            url: '<?php echo U("pay/index");?>',
            data: data,
            dataType: 'json',
            success: function (res) {
                alert(res.msg);
            }


        })
    })

</script>