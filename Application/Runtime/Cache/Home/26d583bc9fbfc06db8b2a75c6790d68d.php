<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>注册</title>
    <link rel="stylesheet" href="/Public/Home/css/public.css">
    <script src="/Public/Home/js/jquery-1.12.4.min.js"></script>
    <script src="/Public/Home/js/common.js"></script>
</head>

<body>
    <div class="dz_head">
        <a href="javascript:history.back(-1)"><img src="/Public/Home/img/fh.png"></a>
        <span>注册</span>
    </div>
    <div class="dz_main">
        <form>
            <div>
                <input type="text" name="username" placeholder="用户名" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" required>
            </div>
            <div>
                <input type="text" name="phone" placeholder="手机号码" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" required>
            </div>
            <div>
                <input type="password" name="password" placeholder="密码" onkeyup="this.value=this.value.replace(/[, ]/g,'')" required>
            </div>
            <div>
                <input type="password" name="confirm" placeholder="再次确认密码" onkeyup="this.value=this.value.replace(/[, ]/g,'')" required>
            </div>
            <input id="submit" type="button" value="同意协议并注册">
        </form>
    </div>
</body>
<script>

    function isPoneAvailable(str) {
        var myreg=/^[1][3,4,5,7,8][0-9]{9}$/;
        if (!myreg.test(str)) {
            return false;
        } else {
            return true;
        }
    }

    $("#submit").click(function(){

        var username = $("input[name=username]").val();
        var phone    = $("input[name=phone]").val();
        var password = $("input[name=password]").val();
        var confirm  = $("input[name=confirm]").val();

        if(username==""|phone==""||password==""||confirm==""){
            alert('请填写完数据！！！');
            return;
        }
        if(!isPoneAvailable(phone)){
            alert('手机格式不正确！！！');
            return;
        }

        if(password!==confirm){
            alert('两次密码不相同！！！');
            return;
        }

        $.post("<?php echo U('reg_do')?>",{username:username,password:password,phone:phone,confirm:confirm},function(res){
            alert(res.info); window.location.href=res.url;

        });
    });
</script>
</html>