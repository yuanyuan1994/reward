<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="format-detection" content="telephone=no"/>
    <meta content="email=no" name="format-detection"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>登录</title>
    <link rel="stylesheet" href="/Public/Home/css/public.css">
    <script src="/Public/Home/js/jquery-1.12.4.min.js"></script>
    <script src="/Public/Home/js/common.js"></script>
</head>

<body>
<div class="dz_head">
    <a href="javascript:history.back(-1)"><img src="/Public/Home/img/fh.png"></a>
    <span>手机快捷登录</span>
</div>
<div class="dz_main">
    <form action="">
        <div>
            <input type="text" name="username" placeholder="用户名">
            <input type="hidden" name="code" value="<?php echo ($_GET['code']); ?>">
        </div>
        <div>
            <input type="password" name="password" placeholder="密码">
        </div>
        <input type="button" id="submit" value="登录" style="margin-bottom: 0;">
        <p style="margin: 0.1rem auto;">或</p>
        <input type="button" value="注册"  style="margin-top: 0;border-color: #2692FF;" onclick="reg()">
    </form>
</div>
</body>




</html>
<script>
    var reg = function () {
        location.href = "/Home/Login/reg";
    };
    $("#submit").click(function () {

        var username = $("input[name=username]").val();
        var password = $("input[name=password]").val();
        var code = $("input[name=code]").val();

        $.post("/Home/Login/login_do", {username: username, password: password, code: code}, function (res) {
            if (res.status == 1) {
                alert(res.info);
                window.location.href = res.url;
            } else {
                alert(res.info);
            }
        });
    });

</script>