<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>个人中心</title>
    <link rel="stylesheet" href="/Public/Home/css/public.css">
</head>
<body>
    <div class="dz_head">
        <img src="/Public/Home/img/fh.png" onclick="javascript :history.back(-1)">
        <span>个人中心</span>
    </div>
    <div class="member-info">
        <img src="/Public/Home/img/profile.png" class="portrait fl">
        <ul class="fl">
            <li class="nickname">用户名：<span><?php echo $data['username']?></span></li>
        </ul>
    </div>
    <ul class="way">
        <a href="<?php echo U('recharge');?>" class="line">
            <img src="/Public/Home/img/atm.png">
            <ul>
                <li class="x-title">充值会员</li>
            </ul>
        </a>

        <a href="<?php echo U('uppwd');?>" class="line">
            <img src="/Public/Home/img/pay.png">
            <ul>
                <li class="x-title">修改密码</li>
            </ul>
        </a>
        <a href="<?php echo U('login/logout');?>" class="line">
            <img src="/Public/Home/img/tuichu.png">
            <ul>
                <li class="x-title">退出登录</li>
            </ul>
        </a>
    </ul>
</body>
</html>
<script src="/Public/Home/js/jquery-1.12.4.min.js"></script>
<script src="/Public/Home/js/common.js"></script>