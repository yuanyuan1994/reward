<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>后台管理中心</title>
    <link rel="stylesheet" href="/Public/Admin/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Admin/css/admin.css">
    <script src="/Public/Admin/js/jquery.js"></script>
</head>
<body style="background-color:#f2f9fd;">
<div class="header bg-main">
    <div class="logo margin-big-left fadein-top">
        <h1><img src="/Public/Admin/images/y.jpg" class="radius-circle rotate-hover" height="50" alt="" />后台管理中心</h1>
    </div>
    <div class="head-l"><a class="button button-little bg-green" href="" target="_blank"><span class="icon-home"></span> 前台首页</a> &nbsp;&nbsp;
        <a class="button button-little bg-red" href="login.html"><span class="icon-power-off"></span> 退出登录</a> </div>
</div>
<div class="leftnav">
    <div class="leftnav-title"><strong><span class="icon-list"></span>菜单列表</strong></div>
    <h2><span class="icon-user"></span>成员管理</h2>
    <ul style="display:block">
        <li><a href="info.html" target="right"><span class="icon-caret-right"></span>会员列表</a></li>
        <li><a href="pass.html" target="right"><span class="icon-caret-right"></span>代理列表</a></li>
    </ul>

    <h2><span class="icon-file-video-o"></span>视频管理</h2>
    <ul>
        <li><a href="list.html" target="right"><span class="icon-caret-right"></span>公共片库</a></li>
        <li><a href="add.html" target="right"><span class="icon-caret-right"></span>代理片库</a></li>
        <li><a href="cate.html" target="right"><span class="icon-caret-right"></span>上传本地视频</a></li>
        <li><a href="cate.html" target="right"><span class="icon-caret-right"></span>添加视频外链</a></li>
        <li><a href="cate.html" target="right"><span class="icon-caret-right"></span>推广链接列表</a></li>
    </ul>
    <h2><span class="icon-file-pdf-o"></span>订单管理</h2>
    <ul>
        <li><a href="list.html" target="right"><span class="icon-caret-right"></span>订单列表</a></li>
        <li><a href="add.html" target="right"><span class="icon-caret-right"></span>未结算记录</a></li>
        <li><a href="cate.html" target="right"><span class="icon-caret-right"></span>已结算列表</a></li>
    </ul>
    <h2><span class="icon-codepen"></span>系统配置</h2>
    <ul>
        <li><a href="list.html" target="right"><span class="icon-caret-right"></span>基础配置</a></li>
        <li><a href="add.html" target="right"><span class="icon-caret-right"></span>打赏域名列表</a></li>
        <li><a href="cate.html" target="right"><span class="icon-caret-right"></span>公告列表</a></li>
        <li><a href="cate.html" target="right"><span class="icon-caret-right"></span>投诉列表</a></li>
        <li><a href="cate.html" target="right"><span class="icon-caret-right"></span>扣量列表</a></li>
    </ul>
</div>
<script type="text/javascript">
    $(function(){
        $(".leftnav h2").click(function(){
            $(this).next().slideToggle(200);
            $(this).toggleClass("on");
        })
        $(".leftnav ul li a").click(function(){
            $("#a_leader_txt").text($(this).text());
            $(".leftnav ul li a").removeClass("on");
            $(this).addClass("on");
        })
    });
</script>
<ul class="bread">
    <li><a href="" target="right" class="icon-home"> 首页</a></li>
    <li><a href="##" id="a_leader_txt">网站信息</a></li>
    <li><b>当前语言：</b><span style="color:red;">中文</php></span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;切换语言：<a href="##">中文</a> &nbsp;&nbsp;<a href="##">英文</a> </li>
</ul>
<div class="admin">


<div class="main-div">
    <form name="main_form" method="POST" action="/admin/admin/add" enctype="multipart/form-data">
        <table cellspacing="1" cellpadding="3" width="100%">
            <tr>
                <td class="label">用户名：</td>
                <td>
                    <input  type="text" name="username" value="" />
                </td>
            </tr>
            <tr>
                <td class="label">密码：</td>
                <td>
                    <input type="password" size="25" name="password" />
                </td>
            </tr>
            <tr>
                <td colspan="99" align="center">
                    <input type="submit" class="button" value=" 确定 " />
                    <input type="reset" class="button" value=" 重置 " />
                </td>
            </tr>
        </table>
    </form>
</div>


<script>
</script>

</div>
<div style="text-align:center;">
    <p>来源:<a href="http://www.mycodes.net/" target="_blank">源码之家</a></p>
</div>
</body>
</html>