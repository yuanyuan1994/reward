<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title></title>
    <link rel="stylesheet" href="/Public/Admin/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Admin/css/admin.css">
    <script src="/Public/Admin/js/jquery.js"></script>
    <script src="/Public/Admin/js/pintuer.js"></script>
</head>
<body>

<!--添加短连接按钮-->
<div class="form-div search_form_div" style="float: right; margin-right: 100px;">

    <p><input id="goadd" type="button" value=" 添加广告位 " class="button"/></p>

</div>
<!-- 列表 -->
<div class="list-div" id="listDiv">
    <table class="table table-hover text-center">
        <tr>
            <th>活动图片</th>
            <th>开始时间</th>
            <th>结束时间</th>
            <th width="300">操作</th>
        </tr>
        <?php foreach ($data as $k => $v): ?>
        <tr class="tron">
            <td><?php showimage($v['image'],400,50) ?></td>
            <td><?php echo date('Y-m-d H:i:s',$v['start_time']); ?></td>
            <td><?php echo date('Y-m-d H:i:s',$v['end_time']); ?></td>
            <td align="center">
                <a href="<?php echo U('edit?id='.$v['id'].'&p='.I('get.p')); ?>" title="编辑"
                   class="button border-main" style="cursor: pointer"><span class="icon-edit"></span>编辑</a>
                <a class="button border-red del" style="cursor: pointer" onclick="del(this)"
                   url="<?php echo U('delete?id='.$v['id'].'&p='.I('get.p')); ?>"
                   title="移除"> <span class="icon-trash-o"></span>移除</a>
            </td>
        </tr>
        <?php endforeach; ?>
        <?php if(preg_match('/\d/', $page)): ?>
        <tr>
            <td colspan="10">
                <?php echo $page; ?>
            </td>
        </tr>
        <?php endif; ?>
    </table>
</div>

<script>
    $("#goadd").click(function () {
        location.href = "<?php echo U('advert/add')?>";
    })

    function del(obj) {
        if (confirm('确认删除吗？')) {
            $.get($(obj).attr('url'), function (res) {
                alert(res.info);
                location.href = res.url;
            })
        }
    }

</script>

</body>
</html>