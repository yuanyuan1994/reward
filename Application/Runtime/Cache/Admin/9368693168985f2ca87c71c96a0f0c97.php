<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title></title>
    <link rel="stylesheet" href="/Public/Admin/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Admin/css/admin.css">
    <script src="/Public/Admin/js/jquery.js"></script>
    <script src="/Public/Admin/js/pintuer.js"></script>
</head>
<body>


<!-- 搜索 -->
<div class="form-div search_form_div">
    <form action="/index.php/Admin/Domain/lst" method="GET" name="search_form">
        <p>
            域名状态：
            <input type="radio" name="zt" value="1">未分配
            <input type="radio" name="zt" value="2">已分配
        </p>
        <p><input type="submit" value=" 搜索 " class="button"/></p>
    </form>
</div>
<!-- 列表 -->
<!--添加短连接按钮-->
<div class="form-div search_form_div" style="float: right; margin-right: 100px;">

    <p><input id="goadd" type="button" value=" 添加域名 " class="button"/></p>

</div>
<div class="list-div" id="listDiv">
    <table class="table table-hover text-center">
        <tr>
            <th>域名</th>
            <th>所属代理</th>
            <th>域名状态</th>
            <th width="300">操作</th>
        </tr>
        <?php foreach ($data as $k => $v): ?>
        <tr class="tron">
            <td><?php echo $v['domainlink']; ?></td>
            <td><?php echo $v['agentname']; ?></td>
            <td><?php if($v['zt']==1){echo "未分配";}else{echo "已分配";}?></td>
            <td align="center">
                <a href="<?php echo U('edit?id='.$v['id'].'&p='.I('get.p')); ?>" title="编辑"
                   class="button border-main" style="cursor: pointer"><span class="icon-edit"></span>编辑</a>
                <a class="button border-red" style="cursor: pointer" onclick="del(this)"
                   url="<?php echo U('delete?id='.$v['id'].'&p='.I('get.p')); ?>"
                   title="移除"> <span class="icon-trash-o"></span>移除</a>
            </td>
        </tr>
        <?php endforeach; ?>
        <?php if(preg_match('/\d/', $page)): ?>
        <tr>
            <td colspan="10">
                <?php echo $page; ?>
            </td>
        </tr>
        <?php endif; ?>
    </table>
</div>

<script>
    function del(obj) {
        if (confirm('确认删除吗？')) {
            $.get($(obj).attr('url'), function (res) {
                alert(res.info);
                location.href = res.url;
            })
        }
    }

    $("#goadd").click(function () {
        location.href = "<?php echo U('domain/add')?>";
    })
</script>


</body>
</html>