<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title></title>
    <link rel="stylesheet" href="/Public/Admin/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Admin/css/admin.css">
    <script src="/Public/Admin/js/jquery.js"></script>
    <script src="/Public/Admin/js/pintuer.js"></script>
</head>
<body>


<!-- 搜索 -->
<div class="form-div search_form_div">
    <form action="/index.php/Admin/Recharge/lst" method="GET" name="search_form">
        <p>
            充值状态：
            <select name="zt" class="input w35">
                <option value="1">未付款</option>
                <option value="2">已付款</option>
                <option value="3">已取消</option>
            </select>
        </p>
        <p><input type="submit" value=" 搜索 " class="button"/></p>
    </form>
</div>
<!-- 列表 -->
<div class="list-div" id="listDiv">
    <table class="table table-hover text-center">
        <tr>
            <th>ID号</th>
            <th width="300">充值订单号</th>
            <th width="300">充值时间</th>
            <th>充值用户</th>
            <th>充值状态</th>
            <th>充值金额</th>
            <th>vip名称</th>

        </tr>
        <?php foreach ($data as $k => $v): ?>
        <tr class="tron">
            <td><?php echo $v['id']; ?></td>
            <td><?php echo $v['ddh']; ?></td>
            <td><?php echo date('Y-m-d H:i:s',$v['time']); ?></td>
            <td><?php echo $v['username']; ?></td>
            <td><?php if($v['zt']==1){echo '未付款';}elseif($v['zt']==2){echo '已付款';}else{echo '已取消';}; ?></td>
            <td><?php echo $v['money']; ?></td>
            <td><?php echo $v['vipname']; ?></td>
        </tr>
        <?php endforeach; ?>
        <?php if(preg_match('/\d/', $page)): ?>
        <tr>
            <td colspan="10">
                <?php echo $page; ?>
            </td>
        </tr>
        <?php endif; ?>
    </table>
</div>



</body>
</html>