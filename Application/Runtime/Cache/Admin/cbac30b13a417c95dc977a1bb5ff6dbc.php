<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title></title>
    <link rel="stylesheet" href="/Public/Admin/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Admin/css/admin.css">
    <script src="/Public/Admin/js/jquery.js"></script>
    <script src="/Public/Admin/js/pintuer.js"></script>
</head>
<body>

<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>增加内容</strong></div>
    <div class="body-content">
        <form name="main_form" class="form-x" id="myform" method="POST" action="/index.php/Admin/member/add.html" enctype="multipart/form-data">
            <div class="form-group">
                <div class="label">
                    <label>用户名：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="" name="username" data-validate="required:用户名"/>
                    <div class="tips"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    <label>密码：</label>
                </div>
                <div class="field">
                    <input type="password" class="input w50" value="" name="password" data-validate="required:密码"/>
                    <div class="tips"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    <label>手机号：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="" onblur="isPoneAvailable(this.value)" name="phone"
                           data-validate="required:手机号"/>
                    <div class="tips"></div>
                </div>
            </div>
            <input type="hidden" name="domain" value="<?php echo session('admin_name');?>">

            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <input id="submit" type="button" value="提交" class="button bg-blue">
                    <input type="reset" value="重置" class="button bg-red">
                    <input type="button" value="取消" onclick="javascript :history.back(-1)" class="button bg-blue">
                </div>
            </div>
        </form>
    </div>
</div>
<script src="/Public/layui/layui.all.js"></script>
<script>

    $("#submit").click(function () {
        var data = $("#myform").serialize();
        if ($("input[name='username']").val() == '' || $("input[name='username']").val() == undefined) {
            alert('用户名不能为空');
            return false;
        }
        if ($("input[name='password']").val() == '' || $("input[name='password']").val() == undefined) {
            alert('密码不能为空');
            return false;
        }
        if ($("input[name='phone']").val() == '' || $("input[name='phone']").val() == undefined||isPoneAvailable($("input[name='phone']").val())) {
            alert('手机号不能为空');
            return false;
        }

        $.post("<?php echo U('add')?>", data, function (res) {
            alert(res.info);
            location.href = res.url;
        })
    });

    function isPoneAvailable(poneInput) {

        var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
        if (!myreg.test(poneInput)) {
            alert('手机号码不正确！');
            return false;
        }
    }


    //判断用户名是否存在
    $(function () {
        $("input[name='username']").blur(function () {
            $.post('<?php echo U(checkname)?>', {data: $(this).val()}, function (res) {
                if (res == 1) {
                    alert('用户名已存在！，请重新输入');
                    return false;
                }
            });
        });

    });
</script>

</body>
</html>