<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>后台管理中心</title>
    <link rel="stylesheet" href="/Public/Admin/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Admin/css/admin.css">
    <script src="/Public/Admin/js/jquery.js"></script>
</head>
<body style="background-color:#f2f9fd;">
<div class="header bg-main">
    <div class="logo margin-big-left fadein-top">
        <h1><img src="/Public/Admin/images/y.jpg" class="radius-circle rotate-hover" height="50" alt="" />后台管理中心</h1>
    </div>
    <div class="head-l"><a class="button button-little bg-green" href="" target="_blank"><span class="icon-home"></span> 前台首页</a> &nbsp;&nbsp;
        <a class="button button-little bg-red" href="<?php echo U('login/logout');;?>"><span class="icon-power-off"></span> 退出登录</a> </div>
</div>
<div class="leftnav">
    <div class="leftnav-title"><strong><span class="icon-list"></span>菜单列表</strong></div>
    <h2><span class="icon-user"></span>会员管理</h2>
    <ul style="display:block">
        <li><a href="<?php echo U('member/lst');;?>" target="right"><span class="icon-caret-right"></span>会员列表</a></li>
        <li><a href="<?php echo U('member/add');;?>" target="right"><span class="icon-caret-right"></span>添加会员</a></li>
    </ul>
    <h2><span class="icon-user"></span>代理管理</h2>
    <ul style="display:block">
        <li><a href="<?php echo U('agent/lst');;?>"><span class="icon-caret-right"></span>代理列表</a></li>
        <li><a href="<?php echo U('agent/add');;?>"><span class="icon-caret-right"></span>添加代理</a></li>

    </ul>
    <h2><span class="icon-file-video-o"></span>视频管理</h2>
    <ul style="display:block">
        <li><a href="<?php echo U('shipin/lst');;?>" target="right"><span class="icon-caret-right"></span>公共片库</a></li>
        <li><a href="<?php echo U('shipin/dllst');;?>" target="right"><span class="icon-caret-right"></span>代理片库</a></li>
        <li><a href="<?php echo U('shipin/add');;?>" target="right"><span class="icon-caret-right"></span>添加视频外链</a></li>
        <li><a href="<?php echo U('tglink/lst');;?>" target="right"><span class="icon-caret-right"></span>推广链接列表</a></li>
    </ul>
    <h2><span class="icon-file-pdf-o"></span>订单管理</h2>
    <ul style="display:block">
        <li><a href="<?php echo U('txorder/lst');;?>" target="right"><span class="icon-caret-right"></span>订单列表</a></li>
    </ul>
    <h2><span class="icon-codepen"></span>系统配置</h2>
    <ul style="display:block">
        <li><a href="<?php echo U('config/lst');;?>" target="right"><span class="icon-caret-right"></span>基础配置</a></li>
        <li><a href="<?php echo U('vip/lst');;?>" target="right"><span class="icon-caret-right"></span>会员充值配置列表</a></li>
        <li><a href="<?php echo U('domain/lst');;?>" target="right"><span class="icon-caret-right"></span>打赏域名列表</a></li>
    </ul>
</div>
<script type="text/javascript">
    $(function(){
        $(".leftnav h2").click(function(){
            $(this).next().slideToggle(200);
            $(this).toggleClass("on");
        })
        $(".leftnav ul li a").click(function(){
            $("#a_leader_txt").text($(this).text());
            $(".leftnav ul li a").removeClass("on");
            $(this).addClass("on");
        })
    });
</script>
<ul class="bread">
    <li><a href="<?php echo U('index/index');;?>" target="right" class="icon-home"> 首页</a></li>
    <li>网站信息</li>
</ul>
<div class="admin">




<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>增加内容</strong></div>
    <div class="body-content">
        <form name="main_form" class="form-x" id="myform" method="POST" action="/index.php/Admin/Vip/edit/id/4/p/1.html" enctype="multipart/form-data">
            <input type="hidden" name="userid" value="admin">
            <div class="form-group">
                <div class="label">
                    <label>会员时长：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="<?php echo $data['name']?>" placeholder="一个月" name="name"
                    />
                    <div class="tips"></div>
                </div>
            </div>
            <input type="hidden" name="id" value="<?php echo I('get.id');?>">
            <div class="form-group" id="sj">
                <div class="label">
                    <label>会员价格：</label>
                </div>
                <div class="field">
                    <input type="text" id="money" placeholder="10"  class="input w35" value="<?php echo $data['money']?>" name="money"/>
                    <label>元</label>
                    <div class="tips"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <input id="submit" type="button" value="提交" class="button bg-blue">
                    <input type="reset" value="重置" class="button bg-red">
                    <input type="button" value="取消" onclick="javascript :history.back(-1)" class="button bg-blue">
                </div>
            </div>
        </form>
    </div>
</div>
<script src="/Public/layui/layui.all.js"></script>
<script>
    $("#submit").click(function () {
        var data = $("#myform").serialize();
        console.log(data);
        if ($("input[name='name']").val() == '' || $("input[name='name']").val() == undefined) {
            alert('会员时长不能为空');
            return false;
        }
        if ($("input[name='money']").val() == '' || $("input[name='money']").val() == undefined) {
            alert('会员价格不能为空');
            return false;
        }
        $.post("<?php echo U('edit')?>", data, function (res) {
            alert(res.info);
            location.href = res.url;
        })
    })

</script>



</div>

</body>
</html>