<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title></title>
    <link rel="stylesheet" href="/Public/Admin/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Admin/css/admin.css">
    <script src="/Public/Admin/js/jquery.js"></script>
    <script src="/Public/Admin/js/pintuer.js"></script>
</head>
<body>



<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>增加内容</strong></div>
    <div class="body-content">
        <form name="main_form" class="form-x" id="myform" method="POST" action="/Admin/Shipin/edit/id/16.html" enctype="multipart/form-data">

            <div class="form-group">
                <div class="label">
                    <label>视频分类：</label>
                </div>
                <select class="input" style="width:200px; line-height:17px;" name="sptype">
                    <option value="">请选择视频分类</option>
                    <?php if(is_array($sptypedata)): foreach($sptypedata as $key=>$v): ?><option value="<?php echo ($v['id']); ?>"
                        <?php if($data['sptype']==$v['id']){echo 'selected';}?>><?php echo ($v['name']); ?></option><?php endforeach; endif; ?>
                </select>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>视频名称：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="<?php echo $data['name']; ?>" name="name"
                           data-validate="required:视频名称"/>
                    <div class="tips"></div>
                </div>
            </div>
            <?php if($data['sj']==1):?>
            <div class="form-group" id="sj" style="display: none">
                <div class="label">
                    <label>打赏金额：</label>
                </div>
                <div class="field">
                    <input type="text" id="money" class="input w35" value="<?php echo $data['money']; ?>" name=""/>-
                    <input type="text" id="sjm" class="input w35" value="<?php echo $data['sj']; ?>" name="sj"/>
                    <div class="tips"></div>
                </div>
            </div>
            <div class="form-group" id="nosj">
                <div class="label">
                    <label>打赏金额：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="<?php echo $data['money']; ?>" name="money"
                           data-validate="required:打赏金额"/>

                    <div class="tips"></div>
                </div>
            </div>
            <?php else:?>
            <div class="form-group" id="sj">
                <div class="label">
                    <label>打赏金额：</label>
                </div>
                <div class="field">
                    <input type="text" id="money" class="input w35" value="<?php echo $data['money']; ?>" name="money"/>-
                    <input type="text" id="sjm" class="input w35" value="<?php echo $data['sj']; ?>" name="sj"/>
                    <div class="tips"></div>
                </div>
            </div>
            <div class="form-group" id="nosj" style="display: none">
                <div class="label">
                    <label>打赏金额：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="<?php echo $data['money']; ?>" name="money"
                           data-validate="required:打赏金额"/>
                    <div class="tips"></div>
                </div>
            </div>
            <?php endif;?>
            <input type="hidden" id="hidsj" name="" value="">
            <div class="form-group">
                <div class="label">
                    <label>是否开启金额随机：</label>
                </div>
                <div class="field" style="margin-top: 5px;">
                    <input type="checkbox" value="" <?php if($data['sj']!=1)echo "checked";?>>开启随机
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    <label>url地址：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" value="<?php echo $data['url']; ?>" name="url"
                           data-validate="required:url地址"/>
                    <div class="tips"></div>
                </div>
            </div>
            <input type="hidden" name="userid" value="admin">
            <input type="hidden" name="id" value="<?php echo I('get.id');?>">
            <div class="form-group">
                <div class="label">
                    <label>唯一标识：</label>
                </div>
                <div class="field">
                    <input type="text" class="input w50" readonly value="<?php echo $data['zykey']; ?>" name="zykey"
                           data-validate="required:资源key"/>

                    <div class="tips"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    <label>视频封面：</label>

                </div>
                <div style="float: left">
                    <?php showImage($data['smallpic'],50,50);?>
                </div>
                <div class="field">
                    <input type="file" class="input w50" name="vpic"/>
                    <div class="tips"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="label">
                    <label>允许观看模式：</label>
                </div>
                <div class="field" style="margin-top: 5px;">
                    <input type="radio" <?php if ($data['allow']==1) echo 'checked';?> name="allow" value="1">单片观看
                    <input type="radio" <?php if ($data['allow']==2) echo 'checked';?> name="allow" value="2">会员观看
                    <input type="radio" <?php if ($data['allow']==3) echo 'checked';?> name="allow" value="3">两者皆有
                </div>
            </div>
            <div class="form-group">
                <div class="label">
                    <label></label>
                </div>
                <div class="field">
                    <input id="submit" type="submit" value="提交" class="button bg-blue">
                    <input type="reset" value="重置" class="button bg-red">
                    <input type="button" value="取消" onclick="javascript :history.back(-1)" class="button bg-blue">
                </div>
            </div>
        </form>
    </div>
</div>
<script src="/Public/Admin/js/jquery-form.js"></script>
<script src="/Public/Admin/js/ajaxsubmit.js"></script>
<script>
    ajaxsubmit('#submit', "<?php echo U('edit')?>", 'post', 'json', '#myform');
    $('input[type=checkbox]').change(function () {
        if ($(this).prop('checked')) {
            //如果选中则是随机，否则为不随机
            $('#sj').show();
            $('#nosj').hide();
            $('#nosj').find("input[type='text']").attr('name', '');
            $('#sj').find("#money").attr('name', 'money');
            $('#sj').find("#sjm").attr('name', 'sj');
            $("#hidsj").attr('name', '');
        } else {
            $('#nosj').show();
            $('#sj').hide();
            $('#sj').find("input[type='text']").attr('name', '');
            $('#nosj').find("input[type='text']").attr('name', 'money');
            $("#hidsj").attr('name', 'sj').val(1);
        }
    })
</script>


</body>
</html>