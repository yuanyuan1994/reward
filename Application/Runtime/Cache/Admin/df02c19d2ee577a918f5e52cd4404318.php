<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>后台管理中心</title>
    <link rel="stylesheet" href="/Public/Admin/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Admin/css/admin.css">
    <script src="/Public/Admin/js/jquery.js"></script>
    <script src="/Public/layer/jquery.js"></script>
    <script src="/Public/layer/layer.js"></script>

</head>
<body style="background-color:#f2f9fd;">
<div class="header bg-main">
    <div class="logo margin-big-left fadein-top">
        <h1><img src="/Public/Admin/images/y.jpg" class="radius-circle rotate-hover" height="50" alt="" />后台管理中心</h1>
    </div>
    <div class="head-l"><a class="button button-little bg-green" href="" target="_blank"><span class="icon-home"></span> 前台首页</a> &nbsp;&nbsp;
        <a class="button button-little bg-red" href="<?php echo U('login/logout');;?>"><span class="icon-power-off"></span> 退出登录</a> </div>
</div>
<div class="leftnav">
    <div class="leftnav-title"><strong><span class="icon-list"></span>菜单列表</strong></div>
    <h2><span class="icon-user"></span>会员管理</h2>
    <ul style="display:block">
        <li><a href="<?php echo U('member/lst');;?>" ><span class="icon-caret-right"></span>会员列表</a></li>
        <li><a href="<?php echo U('member/add');;?>" ><span class="icon-caret-right"></span>添加会员</a></li>
    </ul>
    <h2><span class="icon-user"></span>代理管理</h2>
    <ul style="display:block">
        <li><a href="<?php echo U('agent/lst');;?>"><span class="icon-caret-right"></span>代理列表</a></li>
        <li><a href="<?php echo U('agent/add');;?>"><span class="icon-caret-right"></span>添加代理</a></li>

    </ul>
    <h2><span class="icon-file-video-o"></span>视频管理</h2>
    <ul style="display:block">
        <li><a href="<?php echo U('shipin/lst');;?>" ><span class="icon-caret-right"></span>公共片库</a></li>
        <li><a href="<?php echo U('shipin/dllst');;?>" ><span class="icon-caret-right"></span>代理片库</a></li>
        <li><a href="<?php echo U('shipin/add');;?>" ><span class="icon-caret-right"></span>添加视频外链</a></li>
        <li><a href="<?php echo U('tglink/lst');;?>" ><span class="icon-caret-right"></span>推广链接列表</a></li>
    </ul>
    <h2><span class="icon-file-pdf-o"></span>订单管理</h2>
    <ul style="display:block">
        <li><a href="<?php echo U('txorder/lst');;?>" ><span class="icon-caret-right"></span>订单列表</a></li>
    </ul>
    <h2><span class="icon-file-pdf-o"></span>收入明细</h2>
    <ul style="display:block">
        <li><a href="<?php echo U('recharge/lst');;?>" ><span class="icon-caret-right"></span>会员充值记录</a></li>
    </ul>
    <h2><span class="icon-codepen"></span>系统配置</h2>
    <ul style="display:block">
        <li><a href="<?php echo U('config/lst');;?>" ><span class="icon-caret-right"></span>基础配置</a></li>
        <li><a href="<?php echo U('sptype/lst');;?>" ><span class="icon-caret-right"></span>视频分类</a></li>
        <li><a href="<?php echo U('vip/lst');;?>" ><span class="icon-caret-right"></span>会员充值配置列表</a></li>
        <li><a href="<?php echo U('domain/lst');;?>" ><span class="icon-caret-right"></span>打赏域名列表</a></li>
    </ul>
</div>
<script type="text/javascript">
    $(function(){
        $(".leftnav h2").click(function(){
            $(this).next().slideToggle(200);
            $(this).toggleClass("on");
        })
        $(".leftnav ul li a").click(function(){
            $("#a_leader_txt").text($(this).text());
            $(".leftnav ul li a").removeClass("on");
            $(this).addClass("on");
        })
    });
</script>
<ul class="bread">
    <li><a href="<?php echo U('index/index');;?>"  class="icon-home"> 首页</a></li>
    <li>网站信息</li>
</ul>
<div class="admin">



<div class="main-div">


    <form name="main_form" class="form-x" id="myform" method="POST" action="/index.php/Admin/Agent/edit/id/3.html" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $data['id']; ?>"/>
        <div class="form-group">
            <div class="label">
                <label>代理名称：</label>
            </div>
            <div class="field">
                <input type="text" class="input w50" value="<?php echo $data['agentname']; ?>" name="agentname"
                       data-validate="required:代理名称"/>
                <div class="tips"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="label">
                <label>手机号：</label>
            </div>
            <div class="field">
                <input type="text" class="input w50" onblur="isPoneAvailable(this.value)"
                       value="<?php echo $data['phone']; ?>" name="phone"
                       data-validate="required:手机号"/>
                <div class="tips"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="label">
                <label>QQ号：</label>
            </div>
            <div class="field">
                <input type="text" class="input w50" value="<?php echo $data['qq']; ?>" name="qq"
                       data-validate="required:QQ号"/>
                <div class="tips"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="label">
                <label>注册时间：</label>
            </div>
            <div class="field">
                <span class="input w50" style="border: none;"><?php echo date('Y-m-d H:i:s',$data['retime']) ?></span>

                <div class="tips"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="label">
                <label>微信号：</label>
            </div>
            <div class="field">
                <input type="text" class="input w50" value="<?php echo $data['wx']; ?>" name="wx"
                       data-validate="required:微信号"/>
                <div class="tips"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="label">
                <label>提现名称：</label>
            </div>
            <div class="field">
                <input type="text" class="input w50" value="<?php echo $data['txname']; ?>" name="txname"
                       data-validate="required:提现名称"/>
                <div class="tips"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="label">
                <label>提现费率：</label>
            </div>
            <div class="field">
                <input type="text" class="input w50" value="<?php echo $data['txfl']; ?>" name="txfl"
                       data-validate="required:费率"/>
                <div class="tips"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="label">
                <label>当前域名：</label>
            </div>
            <div class="label">
                <label>  <?php echo $data['domain'];?></label>
            </div>

        </div>
        <div class="form-group">

            <div class="label">
                <label>请从以下域名中选择：</label>
            </div>
            <div class="">
                <?php if(!empty($result)):?>
                <select name="domain" class="input" style="width:200px; line-height:17px;">
                    <option value="">请选择域名</option>
                    <?php if(is_array($result)): foreach($result as $key=>$v): ?><option value="<?php echo $v['domainlink'];?>"><?php echo $v['domainlink'];?></option><?php endforeach; endif; ?>
                </select>
                <?php else:?>
                <div class="label">
                    <label>暂无可用域名，请配置</label>
                </div>
                <?php endif;?>
            </div>
        </div>
        <div class="form-group">
            <div class="label">
                <label></label>
            </div>
            <div class="field">
                <button class="button bg-blue" type="submit"> 提交</button>
                <input type="reset" value="重置" class="button bg-red">
                <input type="button" value="取消" onclick="javascript :history.back(-1)" class="button bg-main">
            </div>
        </div>
    </form>
</div>
<script>
    $(":submit").click(function () {
        var data = $("#myform").serialize();
        if ($("input[name='agentname']").val() == '' || $("input[name='agentname']").val() == undefined) {
            alert('代理名称不能为空');
            return false;
        }

        if ($("input[name='phone']").val() == '' || $("input[name='phone']").val() == undefined) {
            alert('手机号不能为空');
            return false;
        }
        if ($("input[name='txname']").val() == '' || $("input[name='txname']").val() == undefined) {
            alert('提现人名称不能为空');
            return false;
        }
        if ($("select").val() == '' || $("select").val() == undefined) {
            alert('域名不能为空');
            return false;
        }
        $.post("<?php echo U('edit')?>", data, function (res) {

            alert(res.info);
            location.href = res.url;
        })
    })

    function isPoneAvailable(poneInput) {
        var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
        if (!myreg.test(poneInput)) {
            alert('手机号码不正确！');
            return false;
        }
    }
</script>


</div>

</body>
</html>