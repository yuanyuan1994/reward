<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title>代理管理中心</title>
    <link rel="stylesheet" href="/Public/Admin/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Admin/css/admin.css">
    <script src="/Public/Admin/js/jquery.js"></script>
    <script src="/Public/layer/jquery.js"></script>
    <script src="/Public/layer/layer.js"></script>
</head>
<body style="background-color:#f2f9fd;">
<div class="header bg-main">
    <div class="logo margin-big-left fadein-top">
        <h1><img src="/Public/Admin/images/y.jpg" class="radius-circle rotate-hover" height="50" alt=""/>代理管理中心</h1>
    </div>
    <div class="head-l"><a class="button button-little bg-green" href="<?php echo U('home/index/index');?>" target="_blank"><span class="icon-home"></span>
        前台首页</a> &nbsp;&nbsp;
        <a class="button button-little bg-red" href="<?php echo U('login/logout');;?>"><span class="icon-power-off"></span>
            退出登录</a></div>
</div>
<div class="leftnav">
    <div class="leftnav-title"><strong><span class="icon-list"></span>菜单列表</strong></div>
    <h2><span class="icon-user"></span>会员管理</h2>
    <ul>
        <li><a href="<?php echo U('member/lst');;?>" target="content"><span class="icon-caret-right"></span>会员列表</a></li>
        <li><a href="<?php echo U('member/add');;?>" target="content"><span class="icon-caret-right"></span>添加会员</a></li>
    </ul>
    <h2><span class="icon-file-video-o"></span>视频管理</h2>
    <ul>
        <li><a href="<?php echo U('shipin/lst');;?>" target="content"><span class="icon-caret-right"></span>公共片库</a></li>
        <li><a href="<?php echo U('shipin/dllst');?>" target="content"><span class="icon-caret-right"></span>私有片库</a></li>
        <li><a href="<?php echo U('shipin/add');;?>" target="content"><span class="icon-caret-right"></span>添加视频外链</a></li>
        <li><a href="<?php echo U('tglink/lst');;?>" target="content"><span class="icon-caret-right"></span>推广链接列表</a></li>
    </ul>
    <h2><span class="icon-file-pdf-o"></span>收入明细</h2>
    <ul>
        <li><a href="<?php echo U('recharge/lst');;?>" target="content"><span class="icon-caret-right"></span>会员充值记录</a></li>
    </ul>
    <h2><span class="icon-file-pdf-o"></span>财务管理</h2>
    <ul>
        <li><a href="<?php echo U('txorder/add');;?>" target="content"><span class="icon-caret-right"></span>申请提现</a></li>
        <li><a href="<?php echo U('txorder/lst');;?>" target="content"><span class="icon-caret-right"></span>提现记录</a></li>
    </ul>
</div>
<script type="text/javascript">
    $(function () {
        $(".leftnav h2").click(function () {
            $(this).siblings('ul').slideUp(200).next('ul').slideDown(200);
            if ($(this).next('ul').is(':visible')) {
                $(this).next('ul').slideUp();
                $(this).addClass("on");
            } else {
                $(this).next('ul').slideDown();
                $(this).removeClass("on");
            }
        });


        $(".leftnav ul li a").click(function () {
            $("#a_leader_txt").text($(this).text());
            $(".leftnav ul li a").removeClass("on");
            $(this).addClass("on");
        });
    });
</script>
<ul class="bread">
    <li><a href="<?php echo U('index/index');;?>" class="icon-home"> 首页</a></li>
    <li>网站信息</li>
</ul>
<div class="admin">

    <iframe name="content" src="main.html" frameborder="0" id="mainframe" scrolling="yes" marginheight="0"
            marginwidth="0" width="100%" style="height: 700px;"></iframe>

</div>

</body>
</html>