<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title></title>
    <link rel="stylesheet" href="/Public/Admin/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Admin/css/admin.css">
    <script src="/Public/Admin/js/jquery.js"></script>
    <script src="/Public/Admin/js/pintuer.js"></script>
</head>
<body>


<!-- 搜索 -->
<div class="form-div search_form_div">
    <form action="/index.php/Agent/Member/lst" method="GET" name="search_form">
        <p>
            用户名：
            <input type="text" name="username" size="30" value="<?php echo I('get.username'); ?>"/>
        </p>
        <p>
            手机号：
            <input type="text" name="phone" size="30" value="<?php echo I('get.phone'); ?>"/>
        </p>
        <p><input type="submit" value=" 搜索 " class="button"/></p>
    </form>
</div>
<!-- 列表 -->
<div class="list-div" id="listDiv">
    <table class="table table-hover text-center">
        <tr><th>ID</th>
            <th>用户名</th>
            <th>手机号</th>
            <th>注册时间</th>
            <th>会员类型ID</th>
            <th>会员过期时间</th>
        </tr>
        <?php foreach ($data as $k => $v): ?>
        <tr class="tron">
            <td><?php echo $v['id']; ?></td>
            <td><?php echo $v['username']; ?></td>
            <td><?php echo $v['phone']; ?></td>
            <td><?php echo date('Y-m-d H:i:s',$v['retime']);?></td>
            <td><?php echo $v['vipid']; ?></td>
            <td><?php echo date('Y-m-d H:i:s',$v['viptime']);?></td>
        </tr>
        <?php endforeach; ?>
        <?php if(preg_match('/\d/', $page)): ?>
        <tr>
            <td colspan="10">
                <?php echo $page; ?>
            </td>
        </tr>
        <?php endif; ?>
    </table>
</div>
<script>

</script>
</body>
</html>