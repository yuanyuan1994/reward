<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title></title>
    <link rel="stylesheet" href="/Public/Admin/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Admin/css/admin.css">
    <script src="/Public/Admin/js/jquery.js"></script>
    <script src="/Public/Admin/js/pintuer.js"></script>
</head>
<body>

<div class="panel admin-panel">
    <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>添加短连接</strong></div>

    <form name="main_form" class="form-x" id="myform" method="POST" action="/index.php/Agent/tglink/add.html" enctype="multipart/form-data">
        <table class="table table-hover text-center">
            <tr>
                <th></th>
                <th>资源名称</th>
                <th>资源key</th>
                <th>资源地址</th>
            </tr>
            <?php if(is_array($spdata)): foreach($spdata as $key=>$vo): ?><tr class="tron">
                    <td><input type="checkbox" value="<?php echo ($vo["id"]); ?>" name="check"></td>
                    <td><?php echo ($vo["name"]); ?></td>
                    <td class="zykey"><?php echo ($vo["zykey"]); ?></td>
                    <td class="url"><?php echo ($vo["url"]); ?></td>
                </tr><?php endforeach; endif; ?>
        </table>
    </form>

</div>
<button id="build" class="button border-main">一键生成短连接</button>

<textarea style="display: inline;margin: 15px 0 0 15px;" name="" id="shorturl" cols="30" rows="8"></textarea>

<script src="/Public/layui/layui.all.js"></script>
<script>

    $("#submit").click(function () {
        var data = $("#myform").serialize();

        $.post("<?php echo U('add')?>", data, function (res) {
            alert(res.info);
            location.href = res.url;
        })
    });

    var shortlink = [];
    $("#build").click(function () {
        if ($("input[name='check']:checked").length == 0) {
            alert('还未选中!');
            return false;
        } else {

            $("input[name='check']:checked").each(function () {
                shortlink.push($(this).val());
            });
            $.post('<?php echo U(add)?>', {data: shortlink}, function (res) {
                var res = eval(res);
                var htm = '';
                for (var i = 0; i < res.length; i++) {
                    htm += res[i].shortlink + '\n';
                }
                $("#shorturl").val(htm);
                shortlink = [];
            })

        }

    })


</script>


</body>
</html>