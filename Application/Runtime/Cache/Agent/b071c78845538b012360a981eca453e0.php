<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title></title>
    <link rel="stylesheet" href="/Public/Admin/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Admin/css/admin.css">
    <script src="/Public/Admin/js/jquery.js"></script>
    <script src="/Public/Admin/js/pintuer.js"></script>
</head>
<body>


<!-- 搜索 -->
<div class="form-div search_form_div">
    <form action="/index.php/Agent/Tglink/lst" method="GET" name="search_form">

        <p>
            资源名称：
            <input type="text" name="name" size="30" value="<?php echo I('get.name'); ?>"/>
        </p>
        <p><input type="submit" value=" 搜索 " class="button"/></p>
    </form>
</div>
<!-- 列表 -->
<div class="list-div" id="listDiv">
    <table class="table table-hover text-center">
        <tr>
            <th>ID</th>
            <th>资源名称</th>
            <th>推广链接</th>
            <th>code</th>
            <th width="300">操作</th>
        </tr>
        <?php foreach ($data as $k => $v): ?>
        <tr class="tron">
            <td><?php echo $v['id']; ?></td>
            <td><?php echo $v['name']; ?></td>
            <td><?php echo $v['shortlink']; ?></td>
            <td><?php echo $v['code']; ?></td>
            <td align="center">
                <a class="button border-red del" style="cursor: pointer" onclick="del(this)"
                   url="<?php echo U('delete?id='.$v['id'].'&p='.I('get.p')); ?>"
                   title="移除"> <span class="icon-trash-o"></span>移除</a>
            </td>
        </tr>
        <?php endforeach; ?>
        <?php if(preg_match('/\d/', $page)): ?>
        <tr>
            <td colspan="10">
                <?php echo $page; ?>
            </td>
        </tr>
        <?php endif; ?>
    </table>
</div>
<script>
    function del(obj) {
        if (confirm('确认删除吗？')) {
            $.get($(obj).attr('url'), function (res) {
                alert(res.info);
                location.href = res.url;
            })
        }
    }

</script>
</body>
</html>