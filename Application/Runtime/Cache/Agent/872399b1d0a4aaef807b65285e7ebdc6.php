<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title></title>
    <link rel="stylesheet" href="/Public/Admin/css/pintuer.css">
    <link rel="stylesheet" href="/Public/Admin/css/admin.css">
    <script src="/Public/Admin/js/jquery.js"></script>
    <script src="/Public/Admin/js/pintuer.js"></script>
</head>
<body>


<!-- 搜索 -->
<div class="form-div search_form_div">
    <form action="/Agent/Txorder/lst" method="GET" name="search_form">
        <p>
            订单号：
            <input type="text" name="ddh" size="30" value="<?php echo I('get.ddh'); ?>"/>
        </p>
        <p>
            状&nbsp;&nbsp;&nbsp;&nbsp;态：
            <input type="text" name="zt" size="30" value="<?php echo I('get.zt'); ?>"/>
        </p>
        <p><input type="submit" value=" 搜索 " class="button"/></p>
    </form>
</div>
<!-- 列表 -->
<div class="list-div" id="listDiv">
    <table class="table table-hover text-center">
        <tr>
            <th>订单号</th>
            <th>申请时间</th>
            <th>提款人姓名</th>
            <th>提现费率</th>
            <th>提现金额</th>
            <th>状态</th>
            <th width="300">操作</th>
        </tr>
        <?php foreach ($data as $k => $v): ?>
        <tr class="tron">
            <td><?php echo $v['ddh']; ?></td>
            <td><?php echo date('Y-m-d H:i:s',$v['time']); ?></td>
            <td><?php echo $v['name']; ?></td>
            <td><?php echo $v['txfl']; ?></td>
            <td><?php echo $v['money']; ?></td>
            <td>
                <?php if($v['zt']==1){echo "未结算";}elseif($v['zt']==2){echo "已结算";}elseif($v['zt']==3){ echo "已驳回";}else{echo "已取消 ";} ?>

            </td>
            <td align="center">
                <?php if($v['zt']==1){?>
                <a class="button border-red del" style="cursor: pointer" onclick="change(this)"
                   url="<?php echo U('edit?id='.$v['id'].'&p='.I('get.p')); ?>"
                   title="确认结算"> <span class="icon-trash-o"></span>取消申请</a>
                <?php }?>
            </td>
        </tr>
        <?php endforeach; ?>
        <?php if(preg_match('/\d/', $page)): ?>
        <tr>
            <td align="right" nowrap="true" colspan="99" height="30"><?php echo $page; ?></td>
        </tr>
        <?php endif; ?>
    </table>
</div>

<script>
    function change(obj) {
        if (confirm('确认取消申请吗？')) {
            $.get($(obj).attr('url'), function (res) {
                alert(res.info);
                location.href = res.url;
            })
        }
    }
</script>


</body>
</html>