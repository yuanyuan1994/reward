<?php
namespace Admin\Model;
use Think\Model;
class RechargeModel extends Model 
{
	protected $insertFields = array('ddh','time','userid','zt','money','vipname');
	protected $updateFields = array('id','ddh','time','userid','zt','money','vipname');
	protected $_validate = array(
		array('ddh', 'require', '充值订单号不能为空！', 1, 'regex', 3),
		array('ddh', '1,255', '充值订单号的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('time', 'require', '充值时间不能为空！', 1, 'regex', 1),
		array('userid', 'require', '充值用户ID不能为空！', 1, 'regex', 3),
		array('userid', 'number', '充值用户ID必须是一个整数！', 1, 'regex', 3),
		array('zt', 'require', '充值状态不能为空！', 1, 'regex', 3),
		array('zt', '1,25', '充值状态的值最长不能超过 25 个字符！', 1, 'length', 3),
		array('money', 'require', '充值金额不能为空！', 1, 'regex', 3),
		array('vipname', 'require', 'vip名称不能为空！', 1, 'regex', 3),
		array('vipname', '1,255', 'vip名称的值最长不能超过 255 个字符！', 1, 'length', 3),
	);
	public function search($pageSize = 20)
	{
		/**************************************** 搜索 ****************************************/
		$where = array();
		if($zt = I('get.zt'))
			$where['zt'] = array('like', "%$zt%");
		/************************************* 翻页 ****************************************/
		$count = $this->alias('a')->where($where)->count();
		$page = new \Think\Page($count, $pageSize);
		// 配置翻页的样式
		$page->setConfig('prev', '上一页');
		$page->setConfig('next', '下一页');
		$data['page'] = $page->show();
		/************************************** 取数据 ******************************************/
		$data['data'] = $this->alias('a')->field('a.*,b.username')->where($where)->join('left join __MEMBER__ b on a.userid=b.id')->group('a.id')->limit($page->firstRow.','.$page->listRows)->select();
		return $data;
	}
	// 添加前
	protected function _before_insert(&$data, $option)
	{
	}
	// 修改前
	protected function _before_update(&$data, $option)
	{
	}
	// 删除前
	protected function _before_delete($option)
	{
		if(is_array($option['where']['id']))
		{
			$this->error = '不支持批量删除';
			return FALSE;
		}
	}
	/************************************ 其他方法 ********************************************/
}