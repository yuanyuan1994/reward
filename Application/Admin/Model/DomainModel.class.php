<?php
namespace Admin\Model;
use Think\Model;
class DomainModel extends Model 
{
	protected $insertFields = array('domainlink','zt');
	protected $updateFields = array('id','domainlink','zt');
	protected $_validate = array(
		array('domainlink', 'require', '域名不能为空！', 1, 'regex', 3),
		array('domainlink', '1,255', '域名的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('zt', '1,255', '域名状态，0未分配，1已分配的值最长不能超过 255 个字符！', 2, 'length', 3),
	);
	public function search($pageSize = 5)
	{
		/**************************************** 搜索 ****************************************/
		$where = array();
		if($zt = I('get.zt'))
			$where['zt'] = array('like', "%$zt%");
		/************************************* 翻页 ****************************************/
		$count = $this->alias('a')->where($where)->count();
		$page = new \Think\Page($count, $pageSize);
		// 配置翻页的样式
		$page->setConfig('prev', '上一页');
		$page->setConfig('next', '下一页');
		$data['page'] = $page->show();
		/************************************** 取数据 ******************************************/
		$data['data'] = $this->alias('a')->field('a.*,b.agentname')->join('left join __AGENT__ b on a.userid=b.id')->where($where)->group('a.id')->limit($page->firstRow.','.$page->listRows)->select();
		return $data;
	}
	// 添加前
	protected function _before_insert(&$data, $option)
	{
	}
	// 修改前
	protected function _before_update(&$data, $option)
	{
	}
	// 删除前
	protected function _before_delete($option)
	{
		if(is_array($option['where']['id']))
		{
			$this->error = '不支持批量删除';
			return FALSE;
		}
	}

	/************************************ 其他方法 ********************************************/
}