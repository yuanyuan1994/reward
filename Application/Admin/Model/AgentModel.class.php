<?php

namespace Admin\Model;

use Think\Model;

class AgentModel extends Model
{
    protected $insertFields = array('agentname', 'password', 'phone', 'retime', 'domain', 'qq', 'wx', 'money', 'txname', 'txfl');
    protected $updateFields = array('id', 'agentname', 'password', 'phone', 'retime', 'domain', 'qq', 'wx', 'money', 'txname', 'txfl');
    protected $_validate = array(
        array('agentname', 'require', '代理名不能为空！', 1, 'regex', 3),
        array('agentname', '1,18', '代理名最长不能超过 18 个字符！', 1, 'length', 3),
        array('password', 'require', '密码不能为空！', 1, 'regex', 1),
        array('password', '1,32', '密码的值最长不能超过 32 个字符！', 1, 'length', 1),
        array('phone', 'require', '手机号不能为空！', 1, 'regex', 3),
        array('phone', '1,11', '手机号的值最长不能超过 11 个字符！', 1, 'length', 3),
        array('domain', 'require', '所在域名不能为空！', 1, 'regex', 3),
        array('qq', '1,20', 'qq号最长不能超过 20 个字符！', 2, 'length', 3),
        array('wx', '1,50', '微信号最长不能超过 50 个字符！', 2, 'length', 3),
        array('money', '1,255', '金额最长不能超过 255 个字符！', 2, 'length', 3),
        array('txname', '1,32', '提现名称最长不能超过 32 个字符！', 2, 'length', 3),
        array('txfl', '1,30', '提现费率最长不能超过 30 个字符！', 2, 'length', 3),
    );

    public function search($pageSize = 5)
    {
        /**************************************** 搜索 ****************************************/
        $where = array();
        if ($agentname = I('get.agentname'))
            $where['agentname'] = array('like', "%$agentname%");
        if ($phone = I('get.phone'))
            $where['phone'] = array('like', "%$phone%");
        if ($domain = I('get.domain'))
            $where['domain'] = array('like', "%$domain%");
        /************************************* 翻页 ****************************************/
        $count = $this->alias('a')->where($where)->count();
        $page = new \Think\Page($count, $pageSize);
        // 配置翻页的样式
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $data['page'] = $page->show();
        /************************************** 取数据 ******************************************/
        $data['data'] = $this->alias('a')->where($where)->group('a.id')->limit($page->firstRow . ',' . $page->listRows)->select();
        return $data;
    }

    // 添加前
    protected function _before_insert(&$data, $option)
    {
        $data['domain'] = str_replace('，', ',', $data['domain']);
        $data['password'] = md5($data['password']);
        $data['retime'] = time();
        $data['domain'] = htmlspecialchars($data['domain']);

    }

    protected function _after_insert(&$data, $option)
    {
        $res = explode(',', $data['domain']);
        $dmodel = M('Domain');
        foreach ($res as $v) {
            $dmodel->where(['domainlink' => $v])->save(['zt' => 2, 'userid' => $data['id']]);
        }
    }

    // 修改前
    protected function _before_update(&$data, $option)
    {

        $data['domain'] = str_replace('，', ',', $data['domain']);
        $data['domain'] = htmlspecialchars($data['domain']);
        if ($data['password'])
            $data['password'] = md5($data['password']);
        else
            unset($data['password']);
    }

    //修改后
    protected function _after_update(&$data, $option)
    {
        $res = explode(',', $data['domain']);
        $dmodel = M('Domain');
        $dmodel->where(['userid' => $data['id']])->save(['zt' => 1, 'userid' => '']);
        foreach ($res as $v) {
            $dmodel->where(['domainlink' => $v])->save(['zt' => 2, 'userid' => $data['id']]);
        }
    }

    // 删除前
    protected function _before_delete($option)
    {

        $dmodel = M('Domain');
        $dmodel->where(['userid' => $option['where']['id']])->save(['zt' => 1, 'userid' => '']);
        if (is_array($option['where']['id'])) {
            $this->error = '不支持批量删除';
            return FALSE;
        }
    }
    /************************************ 其他方法 ********************************************/
}