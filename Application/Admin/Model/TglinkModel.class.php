<?php

namespace Admin\Model;

use Think\Model;

class TglinkModel extends Model
{
    protected $insertFields = array('name', 'link', 'code');
    protected $updateFields = array('id', 'name', 'link', 'code');
    protected $_validate = array(
        array('name', 'require', '资源名称不能为空！', 1, 'regex', 3),
        array('name', '1,50', '资源名称的值最长不能超过 50 个字符！', 1, 'length', 3),
        array('link', 'require', '短链接不能为空！', 1, 'regex', 3),
        array('link', '1,255', '短链接的值最长不能超过 255 个字符！', 1, 'length', 3),
        array('code', 'require', '资源code不能为空！', 1, 'regex', 3),
        array('code', '1,255', '资源code的值最长不能超过 255 个字符！', 1, 'length', 3),
    );

    public function search($pageSize = 5)
    {
        /**************************************** 搜索 ****************************************/
        $where = array();
        if ($name = I('get.name'))
            $where['name'] = array('like', "%$name%");
        /************************************* 翻页 ****************************************/
        $count = $this->alias('a')->where($where)->count();
        $page = new \Think\Page($count, $pageSize);
        // 配置翻页的样式
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $data['page'] = $page->show();
        /************************************** 取数据 ******************************************/
        $data['data'] = $this->alias('a')->where($where)->group('a.id')->limit($page->firstRow . ',' . $page->listRows)->select();
        return $data;
    }

    // 添加前
    protected function _before_insert(&$data, $option)
    {
        //在添加短连接前先将链接进行合成
        var_dump($data);
        exit();
    }

    // 修改前
    protected function _before_update(&$data, $option)
    {
    }

    // 删除前
    protected function _before_delete($option)
    {
        if (is_array($option['where']['id'])) {
            $this->error = '不支持批量删除';
            return FALSE;
        }
    }
    /************************************ 其他方法 ********************************************/
    public function dourl()
    {

    }
}