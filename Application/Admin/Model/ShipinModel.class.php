<?php

namespace Admin\Model;

use Think\Model;

class ShipinModel extends Model
{
    protected $insertFields = array('money', 'sj', 'url', 'userid', 'name', 'zykey', 'allow', 'time', 'sptype', 'bigpic', 'smallpic');
    protected $updateFields = array('id', 'money', 'sj', 'url', 'userid', 'name', 'zykey', 'allow', 'time', 'sptype', 'bigpic', 'smallpic');
    protected $_validate = array(
        array('money', 'require', '打赏金额不能为空！', 1, 'regex', 3),
        array('sptype', 'require', '视频类型不能为空！', 1, 'regex', 3),
        array('money', '1,255', '打赏金额的值最长不能超过 255 个字符！', 1, 'length', 3),
        array('url', 'require', 'url地址不能为空！', 1, 'regex', 3),
        array('url', '1,255', 'url地址的值最长不能超过 255 个字符！', 1, 'length', 3),
        array('userid', 'require', '发布者id不能为空！', 1, 'regex', 3),
        array('userid', '1,255', '发布者id的值最长不能超过 255 个字符！', 1, 'length', 3),
        array('name', 'require', '资源名称不能为空！', 1, 'regex', 3),
        array('name', '1,255', '资源名称的值最长不能超过 255 个字符！', 1, 'length', 3),
        array('zykey', 'require', '资源key不能为空！', 1, 'regex', 3),
        array('zykey', '1,1000', '资源key的值最长不能超过 1000 个字符！', 1, 'length', 3),
        array('allow', '1,4', '允许观看模式,1为单片观看，2位包月观看，1,2位两者皆有的值最长不能超过 4 个字符！', 2, 'length', 3),
    );

    public function search($pageSize = 5)
    {
        /**************************************** 搜索 ****************************************/
        $where = array();
        if ($userid = I('get.userid'))
            $where['userid'] = array('like', "%$userid%");
        if ($name = I('get.name'))
            $where['name'] = array('like', "%$name%");
        if ($allow = I('get.allow'))
            $where['allow'] = array('like', "%$allow%");
        if ($sptype = I('get.sptype'))
            $where['sptype'] = array('like', "%$sptype%");
        $where['userid'] = array('eq', "admin");

        /************************************* 翻页 ****************************************/
        $count = $this->alias('a')->where($where)->count();
        $page = new \Think\Page($count, $pageSize);
        // 配置翻页的样式
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $data['page'] = $page->show();
        /************************************** 取数据 ******************************************/
        $data['data'] = $this->alias('a')->field('a.*,b.name as typename')->join('left join __SPTYPE__ b on a.sptype=b.id')->where($where)->group('a.id')->limit($page->firstRow . ',' . $page->listRows)->select();
        return $data;
    }

//代理视频库
    public function dllst($pageSize = 5)
    {
        /**************************************** 搜索 ****************************************/
        $where = array();
        if ($name = I('get.name'))
            $where['name'] = array('like', "%$name%");
        if ($allow = I('get.allow'))
            $where['allow'] = array('like', "%$allow%");
        if ($sptype = I('get.sptype'))
            $where['sptype'] = array('like', "%$sptype%");
        /************************************* 翻页 ****************************************/
        $where['userid'] = array('NEQ', 'admin');
        $count = $this->alias('a')->where($where)->count();
        $page = new \Think\Page($count, $pageSize);
        // 配置翻页的样式
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $data['page'] = $page->show();
        /************************************** 取数据 ******************************************/
        $data['data'] = $this->alias('a')->field('a.*,b.name as typename')->join('left join __SPTYPE__ b on a.sptype=b.id')->where($where)->group('a.id')->limit($page->firstRow . ',' . $page->listRows)->select();
        return $data;
    }

    // 添加前
    protected function _before_insert(&$data, $option)
    {
        $data['time'] = time();
        if (empty($data['sj'])) {
            unset($data['sj']);
        }
        if (isset($_FILES['vpic']) && $_FILES['vpic']['error'] == 0) {
            $ret = uploadOne('vpic', 'video', array(
                array(180, 123, 2),
            ));
            if ($ret['ok'] == 1) {
                $data['bigpic'] = $ret['images'][0];
                $data['smallpic'] = $ret['images'][1];
            } else {
                $this->error = $ret['error'];
                return FALSE;
            }
        }

    }

    // 修改前
    protected function _before_update(&$data, $option)
    {

        $id = $option['where']['id'];
        if (isset($_FILES['vpic']) && $_FILES['vpic']['error'] == 0) {

            $ret = uploadOne('vpic', 'video', array(
                array(180, 123, 2),
            ));
            if ($ret['ok'] == 1) {
                $data['bigpic'] = $ret['images'][0];
                $data['smallpic'] = $ret['images'][1];
            } else {
                $this->error = $ret['error'];
                return FALSE;
            }
        }
        $pic = M('shipin')->field('bigpic,smallpic')->where(['id' => $id])->find();
        $ic = C('IMAGE_CONFIG');
        foreach ($pic as $v) {
            unlink($ic['rootPath'] . $v);
        }
    }

    // 删除前
    protected function _before_delete($option)
    {
        $id = $option['where']['id'];
        if (is_array($option['where']['id'])) {
            $this->error = '不支持批量删除';
            return FALSE;
        }
        $pic = M('shipin')->field('bigpic,smallpic')->where(['id' => $id])->find();
        $ic = C('IMAGE_CONFIG');
        foreach ($pic as $v) {
            unlink($ic['rootPath'] . $v);
        }
    }
    /************************************ 其他方法 ********************************************/
}