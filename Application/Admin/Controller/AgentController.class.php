<?php

namespace Admin\Controller;

use Think\Controller;

class AgentController extends CommonController
{
    public function add()
    {

        if (IS_POST) {
            $model = D('Agent');
            if ($model->create(I('post.'), 1)) {
                if ($id = $model->add()) {
                    $this->success('添加成功！', U('lst?p=' . I('get.p')), true);
                    exit;
                }
            }
            $this->error($model->getError(), '', true);
        }
        $domainmodel = M('Domain');
        $result = $domainmodel->where(['zt' => 1])->select();
        $this->assign([
            'result' => $result,
        ]);

        $this->display();
    }

    public function edit()
    {
        $id = I('get.id');
        if (IS_POST) {
            $model = D('Agent');
            if ($model->create(I('post.'), 2)) {
                if ($model->save() !== FALSE) {
                    $this->success('修改成功！', U('lst', array('p' => I('get.p', 1))));
                    exit;
                }
            }
            $this->error($model->getError());
        }
        $model = M('Agent');
        $data = $model->find($id);
        $domainmodel = M('Domain');
        $result = $domainmodel->where(['zt' => 1])->select();
        $this->assign([
            'result' => $result,
            'data' => $data,
        ]);

        $this->display();
    }

    public function delete()
    {
        $model = D('Agent');
        if ($model->delete(I('get.id', 0)) !== FALSE) {
            $this->success('删除成功！', U('lst', array('p' => I('get.p', 1))), true);
            exit;
        } else {
            $this->error($model->getError(), '', true);
        }
    }

    public function lst()
    {
        $model = D('Agent');
        $data = $model->search();
        $this->assign(array(
            'data' => $data['data'],
            'page' => $data['page'],
        ));


        $this->display();
    }

    public function checkname($data)
    {
        $model = M('Agent');
        $res = $model->where(['agentname' => $data])->field('id')->find();
        if ($res) {
            echo json_encode(1);
            exit();
        }
    }
}