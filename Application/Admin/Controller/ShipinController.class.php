<?php

namespace Admin\Controller;

use Think\Controller;

class ShipinController extends CommonController
{
    public function add()
    {

        if (IS_POST) {
            $model = D('Shipin');
            if ($model->create(I('post.'), 1)) {
                if ($id = $model->add()) {
                    $this->success('添加成功！', U('lst?p=' . I('get.p')));
                    exit;
                }
            }
            $this->error($model->getError());
        }
        $sptype = M('sptype');
        $data = $sptype->select();

        // 设置页面中的信息
        $this->assign(array(
            'data' => $data,
        ));
        $this->display();
    }

    public function edit()
    {
        $id = I('get.id');
        if (IS_POST) {
            $model = D('Shipin');
            if ($model->create(I('post.'), 2)) {
                if ($model->save() !== FALSE) {
                    $this->success('修改成功！', U('lst', array('p' => I('get.p', 1))));
                    exit;
                }
            }
            $this->error($model->getError());
        }
        $model = M('Shipin');
        $data = $model->find($id);
        $sptype = M('sptype');
        $data1 = $sptype->select();
        $this->assign(array('data' => $data, 'sptypedata' => $data1));
        $this->display();
    }

    public function delete()
    {
        $model = D('Shipin');
        if ($model->delete(I('get.id', 0)) !== FALSE) {
            $this->success('删除成功！', U('lst', array('p' => I('get.p', 1))));
            exit;
        } else {
            $this->error($model->getError());
        }
    }

    public function lst()
    {
        $model = D('Shipin');
        $data = $model->search();

        $sptype = M('sptype');
        $data2 = $sptype->select();
        $this->assign(array(
            'data' => $data['data'],
            'page' => $data['page'],
            'sptypedata' => $data2,

        ));


        $this->display();

    }

    public function dllst()
    {
        $model = D('Shipin');
        $data = $model->dllst();
        $sptype = M('sptype');
        $data2 = $sptype->select();
        $this->assign(array(
            'data' => $data['data'],
            'page' => $data['page'],
            'sptypedata' => $data2,
        ));
        $this->display();
    }
}