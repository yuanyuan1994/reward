<?php

namespace Admin\Controller;

use Think\Controller;

class TxorderController extends CommonController
{
    public function add()
    {
        if (IS_POST) {
            $model = D('Txorder');
            if ($model->create(I('post.'), 1)) {
                if ($id = $model->add()) {
                    $this->success('添加成功！', U('lst?p=' . I('get.p')));
                    exit;
                }
            }
            $this->error($model->getError());
        }

        // 设置页面中的信息
        $this->assign(array(
            '_page_title' => '添加',
            '_page_btn_name' => '列表',
            '_page_btn_link' => U('lst'),
        ));
        $this->display();
    }

    public function edit()
    {
        $id = I('get.id');
        $model = M('Txorder');
        $data = ['zt' => '2'];
        $model->where(['id' => $id])->save($data);
        if ($model->where(['id' => $_GET['id']])->save($data) !== FALSE) {
            $this->success('结算成功！', U('lst', array('p' => I('get.p', 1))), true);
            exit;
        } else {
            $this->error($model->getError(), '', true);
        }
        $this->display();
    }

    public function del()
    {
        $id = I('get.id');
        $model = M('Txorder');
        $data = ['zt' => '3'];
        $model->where(['id' => $id])->save($data);
        if ($model->where(['id' => $_GET['id']])->save($data) !== FALSE) {
            $this->success('驳回成功！', U('lst', array('p' => I('get.p', 1))), true);
            exit;
        } else {
            $this->error($model->getError(), '', true);
        }
    }

    public function lst()
    {
        $model = D('Txorder');
        $data = $model->search();
        $this->assign(array(
            'data' => $data['data'],
            'page' => $data['page'],
        ));

        // 设置页面中的信息
        $this->assign(array(
            '_page_title' => '列表',
            '_page_btn_name' => '添加',
            '_page_btn_link' => U('add'),
        ));
        $this->display();
    }
}