<?php
namespace Admin\Controller;
use Think\Controller;
class SptypeController extends Controller 
{
    public function add()
    {
    	if(IS_POST)
    	{
    		$model = D('Sptype');
    		if($model->create(I('post.'), 1))
    		{
    			if($id = $model->add())
    			{
    				$this->success('添加成功！', U('lst?p='.I('get.p')),true);
    				exit;
    			}
    		}
    		$this->error($model->getError());
    	}


		$this->display();
    }
    public function edit()
    {
    	$id = I('get.id');
    	if(IS_POST)
    	{
    		$model = D('Sptype');
    		if($model->create(I('post.'), 2))
    		{
    			if($model->save() !== FALSE)
    			{
    				$this->success('修改成功！', U('lst', array('p' => I('get.p', 1))),true);
    				exit;
    			}
    		}
    		$this->error($model->getError());
    	}
    	$model = M('Sptype');
    	$data = $model->find($id);
    	$this->assign('data', $data);

		$this->display();
    }
    public function delete()
    {
    	$model = D('Sptype');
    	if($model->delete(I('get.id', 0)) !== FALSE)
    	{
    		$this->success('删除成功！', U('lst', array('p' => I('get.p', 1))),true);
    		exit;
    	}
    	else 
    	{
    		$this->error($model->getError(),'',true);
    	}
    }
    public function lst()
    {
    	$model = D('Sptype');
    	$data = $model->search();
    	$this->assign(array(
    		'data' => $data['data'],
    		'page' => $data['page'],
    	));

    	$this->display();
    }
}