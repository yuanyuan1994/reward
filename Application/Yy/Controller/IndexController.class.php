<?php

namespace Yy\Controller;

use Home\Controller\CommonController;
use Yy\Model\IndexModel;

class IndexController extends CommonController
{
    public function index()
    {
        $merchant = C('MERCHANT');
        $payserverUrl = C('PAYSERVERURL');
        $curlPost = C('CURLPOST');
        $key = C('KEY');
        $callBack = C('CALLBACK');

        $module = $_POST['module'];
        $payer = $_POST['payer'];
        $module_array = array("1", "2");

        if (in_array($module, $module_array) == false) exit;

        $pay_array = array(
            'alipay' => "ali",
            'wxpay' => "weixin",
            'qqpay' => "qq"
        );
        if (array_key_exists($payer, $pay_array) == false) exit;
        $module = (int)$module;
        $payname_array = array(
            'alipay' => "支付宝",
            'wxpay' => "微信",
            'qqpay' => "QQ"
        );


        $username = $_POST['username'];
        $uid = intval($_POST['uid']);
        $payamount = sprintf("%.2f", $_POST["amount"]);
        if ($payamount <= 0) {
            exit;
        }


        $sn = getsn($merchant);
        $deviceInfo = $uid;
        $totalAmount = $payamount;
        $subject = "用户名 " . $username . " " . $payname_array[$payer] . "充值 " . $totalAmount . " 元";
        $tradeIP = get_ip();
        $remark = $subject;
        $channel = $pay_array[$payer];
        $terminalType = "mobile";
        if ($module == 1) {
            $busiType = '100001';
            include 'ScanCode.php';
        } elseif ($module == 2) {
            $busiType = '120001';
            include 'h5.php';
        }
        $model = D('Index');
        $curlPost = $model->ceshi($sn, $merchant, $deviceInfo, $totalAmount, $subject, $callBack, $channel, $remark, $key, $busiType, $terminalType, $tradeIP);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $payserverUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);//单位 秒，也可以使用

//设置是通过post还是get方法
        curl_setopt($ch, CURLOPT_POST, 1);
//传递的变量
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch); // 请求返回值

        curl_close($ch);

        $dataJson = json_decode($data);
        $codeRsp = $dataJson->codeRsp;
        $codeRsp = json_encode($codeRsp);
        $encrypt = $dataJson->encrypt;
        $codeRsp = json_decode($codeRsp, true); //再把json字符串格式化为数组
        //echo var_dump($codeRsp).'</br>';exit;
        //echo $codeRsp["aliPayURL"]; //获取H5支付URL
        //echo $codeRsp["qrCode"];exit; //获取扫码支付URL

        if ($module == 1) {
            if (empty($codeRsp["qrCode"])) {
                $arr = array('status' => 'FFFF', 'msg' => '支付通道繁忙，请稍后再试');
                echo json_encode($arr);
                exit;
            }
            if (isMobile()) {
                header("Location: " . $codeRsp["qrCode"]);
            } /*elseif (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $sign = md5($payname_array[$payer] . $codeRsp["qrCode"] . date("Y-m-d"));
        $payer = $payname_array[$payer];
        $codeurl = $codeRsp["qrCode"];
        if (!$codeurl) {
            exit();
        } elseif ($sign != md5($payer . $codeurl . date("Y-m-d"))) {
            exit;
        } else {
            include "./phpqrcode.php";
            $errorCorrectionLevel = "L"; // 纠错级别：L、M、Q、H
            $matrixPointSize = "4"; // 点的大小：1到10
            QRcode::png($codeurl, false, $errorCorrectionLevel, $matrixPointSize);
            //echo "?sign=$sign&payer=$payer&url=$codeurl";
        }
    } */
            else {
                header("Location: ScanImg.php?sign=" . md5($payname_array[$payer] . $codeRsp["qrCode"] . date("Y-m-d")) . "&payer=" . $payname_array[$payer] . "&codeurl=" . $codeRsp["qrCode"]);
            }
        } elseif ($module == 2) {
            //echo "<BR><BR>".$codeRsp["aliPayURL"];exit;
            if (empty($codeRsp["aliPayURL"])) {
                $tip = iconv('UTF-8', 'GB2312', '支付通道繁忙，请重新提交充值！');
                echo "<script language=\"javaScript\">alert(\"$tip\");window.close();</script>";
            }
            header("Location: " . $codeRsp["aliPayURL"]);
        }

//encrypt值可能为空
        if ($encrypt) {
            $en_encryptKey = $encrypt->encryptKey;
            $en_encryptData = $encrypt->encryptData;
            //echo 'en_encryptKey:'.$en_encryptKey.'</br>';
            //echo 'en_encryptData:'.$en_encryptData.'</br>';

            $deKeyStr16 = decode_rsa(urldecode($en_encryptKey));
            //echo '</br>'.'deKeyStr16:'.$deKeyStr16.'</br>';

            $deData = decode_aes($deKeyStr16, urldecode($en_encryptData));
            //echo 'deData:'.$deData.'</br>';

            //echo "--:".substr($deData,0,strrpos($deData,'}')+1);
            $dataJsonRs = json_decode(substr($deData, 0, strrpos($deData, '}') + 1));
            //echo 'code:'.$dataJsonRs->code.'</br>';
        }
    }


}
