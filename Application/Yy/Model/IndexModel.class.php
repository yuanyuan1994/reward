<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/8
 * Time: 14:29
 */

namespace Yy\Model;

class IndexModel
{
    public function ceshi($sn, $merchant, $deviceInfo, $totalAmount, $subject, $callBack, $channel, $remark, $key, $busiType, $terminalType, $tradeIP)
    {
        $content = $sn . $merchant . $deviceInfo . $totalAmount . $subject . $callBack . $channel . $remark . $key;
//echo 'md5 sign content:'.$content.'</br>';exit;
        $sign_md5 = md5($content);
//echo 'md5 sign value:'.$sign_md5.'</br>';

        $sign = sign_sha1($sign_md5);
    //echo 'sha1 sign content:'.$sign.'</br>'; exit();//sign value

        $keyStr16 = generate_randsn(16);
//echo 'keyStr16:'.$keyStr16.'</br>';

//$busiType='120001';
        $aes_content = $sign_md5 . $busiType;
//echo 'aes_content:'.$aes_content.'</br>';

        $encryptData = sign_aes($aes_content, $keyStr16);
//echo 'encryptData:'.$encryptData.'</br>'; //encryptData value

        $encryptKey = sign_rsa($keyStr16);
//echo 'encryptKey:'.$encryptKey.'</br>'; //encryptKey value

//echo urlencode($sign).'</br>';;
//echo urlencode($encryptData).'</br>';
//echo urlencode($encryptKey).'</br>';;
//post方式
        $curlPost = 'merchant=' . $merchant . '&sn=' . $sn . '&deviceInfo=' . $deviceInfo . '&totalAmount=' . $totalAmount . '&subject=' . $subject . '&callBack=' . $callBack . '&channel=' . $channel . '&busiType=' . $busiType . '&terminalType=' . $terminalType . '&tradeIP=' . $tradeIP . '&remark=' . $remark . '&sign=' . urlencode($sign) . '&encryptData=' . urlencode($encryptData) . '&encryptKey=' . urlencode($encryptKey);
        return $curlPost;
    }
}