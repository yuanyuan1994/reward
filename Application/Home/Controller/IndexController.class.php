<?php

namespace Home\Controller;
class IndexController extends CommonController
{

    public function index()
    {
        if (I('GET.code')) {
            setcookie('code', I('GET.code'), time() + 3600, '/');
        }
        $spmode = M('Member');
        $domain = $spmode->field('domain,viptime')->where(['id' => session('uid')])->find();//查找当前用户所在的代理id

        $sptypemodel = M('sptype');
        $sptype = $sptypemodel->select();//查找所有的视频类型

        $spmodel = M('shipin');
        foreach ($sptype as $k => $v) {
            $data[$v['name']] = $spmodel->where(['sptype' => $v['id'], ['userid' => $domain['domain']]])->select();
        }
        //循环查出所有类型下的所有视频，并存储在对应的视频类型名称的数组中
        //廣告
        $ggdata = M('advert')->where(['end_time' => ['EGT', time()]])->select();


        $this->assign([
            'ggdata' => $ggdata,
            'viptime' => $domain,
            'data' => $data,
        ]);
        $this->display();
    }

    /**充值**/
    public function recharge()
    {
        $vipmodel = M('vip');
        $data = $vipmodel->order(' id asc ')->select();//查出数据库中的VIP类型

        $this->assign('data', $data);
        $this->display();
    }

    public function info()
    {
        $spmode = M('Member');
        $data = $spmode->where(['id' => session('uid')])->find();//查出用户的信息

        $this->assign([
            'data' => $data,
        ]);
        $this->display();
    }

    public function uppwd()
    {
        if (IS_POST) {
            $old = md5(I('POST.oldpwd'));
            $id = I('POST.id');
            $newpwd = md5(I('POST.password'));
            $model = M('Member');
            $res = $model->field('id')->where(['id' => $id, 'password' => $old])->find();
            if ($res) {
                $dd = $model->where(['id' => $id])->save(['password' => $newpwd]);
                if ($dd) {
                    $this->success('密码修改成功！', '', true);
                } else {
                    $this->error('密码修改失败！', '', true);
                }
            } else {
                $this->error('原始密码不正确！', '', true);
            }
        }
        $this->display();
    }


    public function watch()
    {

        if (I('GET.code')) {
            $code = I('GET.code');
        } elseif (!empty($_COOKIE['code'])) {
            $code = $_COOKIE['code'];
        }
        setcookie('code', $code, time() + 3600, '/');

        $sptypemodel = M('sptype');
        $sptype = $sptypemodel->select();
        $zykey = substr($code, 0, 32);

        $spModel = M('shipin');
        $spdata = $spModel->where(['zykey' => $zykey])->find();
        /*更多推荐*/
        $spmode = M('Member');
        $viptime = $spmode->field('viptime')->where(['id' => session('uid')])->find();
        $sptj = $spmode->alias('a')->field('a.domain,b.*')->join('left join __SHIPIN__ b on a.domain=b.userid')->where(['a.id' => session('uid')])->select();

        $hisdata = M('history')->alias('a')->field('a.*,b.smallpic')->join('left join __SHIPIN__ b on a.spid=b.id')->where(['uid' => session('uid')])->limit(0, 5)->select();
        $this->assign(
            [
                'history' => $hisdata,
                'zydata' => $spdata,
                'sptj' => $sptj,
                'data' => $sptype,
                'viptime' => $viptime,
            ]
        );
        $this->display();
    }

    /**
     * 红包打赏页面
     */
    public function das()
    {
        //根据code获取发布者设置金额范围
        if (I('GET.code')) {
            $code = I('GET.code');
        } elseif (isset($_COOKIE['code'])) {
            $code = $_COOKIE['code'];
        }

        $zykey = substr($code, 0, 32);
        $spModel = M('shipin');
        $spdata = $spModel->field('money,sj')->where(['zykey' => $zykey])->find();
        if ($spdata['sj'] != 1) {
            $money = rand($spdata['money'], $spdata['sj']);
        } else {
            $money = $spdata['money'];
        }
        $this->assign('money', $money);
        $this->display();

    }

    public function history()
    {
        $data = I('POST.');
        $id = M('history')->where($data)->find();
        if (!$id) {
            M('history')->add($data);
        } else {
            exit();
        }
    }
}