<?php

namespace Home\Controller;

use Think\Controller;

class LoginController extends Controller
{

    public function reg()
    {

        $this->display();
    }

    public function reg_do()
    {

        $username = trim($_POST['username']);
        $phone = trim($_POST['phone']);
        $password = trim($_POST['password']);
        $confirm = trim($_POST['confirm']);

        if (empty($username)) {
            $this->error('用户名不能为空！');
        }
        if (empty($phone)) {
            $this->error('手机号不能为空！');
        }
        if (!preg_match("/^1[34578]{1}\d{9}$/", $phone)) {
            $this->error('手机号码格式不正确！');
        }
        if (empty($password)) {
            $this->error('密码不能为空！');
        }
        if (empty($confirm)) {
            $this->error('确认密码不能为空！');
        }
        if ($password !== $confirm) {
            $this->error('两次密码不相同！');
        }
        $domain = $_SERVER['HTTP_ORIGIN'];
        $cfg = M('config');
        $data = $cfg->find();
        if ($domain == $data['zym']) {
            $data = [
                'username' => $username,
                'phone' => $phone,
                'password' => md5($password),
                'domain' => 'admin',
                'retime' => time(),
                'viptime' => time(),
            ];
        } else {
            $domodel = M('domain');
            $userid = $domodel->field('userid')->where(['domainlink' => $domain])->find();
            $data = [
                'username' => $username,
                'phone' => $phone,
                'password' => md5($password),
                'domain' => $userid['userid'],
                'retime' => time(),
                'viptime' => time(),
            ];
        }

        $result = M('member')->add($data);
        if ($result) {
            $this->success('注册成功！！！', U('/Home/Login/login'), true);
        } else {
            $this->error('服务器正忙，请稍后。。。');
        }

    }

    public function login()
    {
        $code = I('get.code');
        $this->assign('code', $code);
        $this->display();
    }

    public function login_do()
    {

        $username = trim($_POST['username']);
        $password = trim($_POST['password']);
        $code = trim($_POST['code']);
        if (empty($username)) {
            $this->error('用户名不能为空！');
        }
        if (empty($password)) {
            $this->error('密码不能为空！');
        }

        $result = M('member')->where(array('username' => $username))->find();
        if ($result) {
            if (md5($password) == $result['password']) {
                //将会员登录信息存入session
                session('member_name', $username);
                session('uid', $result['id']);
                if (!empty($code)) {
                    setcookie('code', $code, time() + 3600, '/');
                    if ($result['viptime'] > time()) {
                        $this->success('登录成功！！', U('/Home/Index/index'), true);
                    } else {
                        $this->success('注意，您的会员已经过期咯！', U('/Home/Index/index'), true);//提示后成跳转至index
                    }
                } else {
                    $this->success('登录成功！！', U('/Home/Index/index'), true);
                }

            } else {

                $this->error('密码错误,请重新输入！');
            }
        } else {
            $this->error('用户名不存在，请重新输入！');
        }


    }

    /**
     * 退出登录处理
     */
    public function logout()
    {
        //清空session
        session('member_name', null);
        session('uid', null);
        setcookie('code', '', time() - 3600, '/');
        $this->redirect('/home/Login/login');

    }
}