<?php
return array(
	'tableName' => 'ds_member',    // 表名
	'tableCnName' => '',  // 表的中文名
	'moduleName' => 'Admin',  // 代码生成到的模块
	'withPrivilege' => FALSE,  // 是否生成相应权限的数据
	'topPriName' => '',        // 顶级权限的名称
	'digui' => 0,             // 是否无限级（递归）
	'diguiName' => '',        // 递归时用来显示的字段的名字，如cat_name（分类名称）
	'pk' => 'id',    // 表中主键字段名称
	/********************* 要生成的模型文件中的代码 ******************************/
	// 添加时允许接收的表单中的字段
	'insertFields' => "array('username','password','phone','retime','vipid','viptime','domain')",
	// 修改时允许接收的表单中的字段
	'updateFields' => "array('id','username','password','phone','retime','vipid','viptime','domain')",
	'validate' => "
		array('username', 'require', '用户名不能为空！', 1, 'regex', 3),
		array('username', '1,18', '用户名的值最长不能超过 18 个字符！', 1, 'length', 3),
		array('password', 'require', '密码不能为空！', 1, 'regex', 3),
		array('password', '1,32', '密码的值最长不能超过 32 个字符！', 1, 'length', 3),
		array('phone', 'require', '手机号不能为空！', 1, 'regex', 3),
		array('phone', '1,11', '手机号的值最长不能超过 11 个字符！', 1, 'length', 3),
		array('retime', 'require', '注册时间不能为空！', 1, 'regex', 3),
		array('vipid', 'require', '会员类型ID不能为空！', 1, 'regex', 3),
		array('vipid', '1,4', '会员类型ID的值最长不能超过 4 个字符！', 1, 'length', 3),
		array('viptime', 'require', '会员过期时间不能为空！', 1, 'regex', 3),
		array('domain', 'require', '会员注册时所在域名位置不能为空！', 1, 'regex', 3),
		array('domain', '1,50', '会员注册时所在域名位置的值最长不能超过 50 个字符！', 1, 'length', 3),
	",
	/********************** 表中每个字段信息的配置 ****************************/
	'fields' => array(
		'username' => array(
			'text' => '用户名',
			'type' => 'text',
			'default' => '',
		),
		'password' => array(
			'text' => '密码',
			'type' => 'password',
			'default' => '',
		),
		'phone' => array(
			'text' => '手机号',
			'type' => 'text',
			'default' => '',
		),
		'retime' => array(
			'text' => '注册时间',
			'type' => 'text',
			'default' => '',
		),
		'vipid' => array(
			'text' => '会员类型ID',
			'type' => 'text',
			'default' => '',
		),
		'viptime' => array(
			'text' => '会员过期时间',
			'type' => 'text',
			'default' => '',
		),
		'domain' => array(
			'text' => '会员注册时所在域名位置',
			'type' => 'textarea',
			'default' => '',
		),
	),
	/**************** 搜索字段的配置 **********************/
	'search' => array(
		array('username', 'normal', '', 'like', '用户名'),
		array('phone', 'normal', '', 'like', '手机号'),
		array('domain', 'normal', '', 'like', '注册时所在域名'),
	),
);