<?php
return array(
	'tableName' => 'ds_tglink',    // 表名
	'tableCnName' => '',  // 表的中文名
	'moduleName' => 'Admin',  // 代码生成到的模块
	'withPrivilege' => FALSE,  // 是否生成相应权限的数据
	'topPriName' => '',        // 顶级权限的名称
	'digui' => 0,             // 是否无限级（递归）
	'diguiName' => '',        // 递归时用来显示的字段的名字，如cat_name（分类名称）
	'pk' => 'id',    // 表中主键字段名称
	/********************* 要生成的模型文件中的代码 ******************************/
	// 添加时允许接收的表单中的字段
	'insertFields' => "array('name','link','code')",
	// 修改时允许接收的表单中的字段
	'updateFields' => "array('id','name','link','code')",
	'validate' => "
		array('name', 'require', '资源名称不能为空！', 1, 'regex', 3),
		array('name', '1,50', '资源名称的值最长不能超过 50 个字符！', 1, 'length', 3),
		array('link', 'require', '短链接不能为空！', 1, 'regex', 3),
		array('link', '1,255', '短链接的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('code', 'require', '资源code不能为空！', 1, 'regex', 3),
		array('code', '1,255', '资源code的值最长不能超过 255 个字符！', 1, 'length', 3),
	",
	/********************** 表中每个字段信息的配置 ****************************/
	'fields' => array(
		'name' => array(
			'text' => '资源名称',
			'type' => 'text',
			'default' => '',
		),
		'link' => array(
			'text' => '短链接',
			'type' => 'text',
			'default' => '',
		),
		'code' => array(
			'text' => '资源code',
			'type' => 'text',
			'default' => '',
		),
	),
	/**************** 搜索字段的配置 **********************/
	'search' => array(
		array('name', 'normal', '', 'like', '资源名称'),
	),
);