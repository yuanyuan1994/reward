<?php
return array(
	'tableName' => 'ds_agent',    // 表名
	'tableCnName' => '',  // 表的中文名
	'moduleName' => 'Admin',  // 代码生成到的模块
	'withPrivilege' => FALSE,  // 是否生成相应权限的数据
	'topPriName' => '',        // 顶级权限的名称
	'digui' => 0,             // 是否无限级（递归）
	'diguiName' => '',        // 递归时用来显示的字段的名字，如cat_name（分类名称）
	'pk' => 'id',    // 表中主键字段名称
	/********************* 要生成的模型文件中的代码 ******************************/
	// 添加时允许接收的表单中的字段
	'insertFields' => "array('agentname','password','phone','retime','domain','qq','wx','money','txname','txfl')",
	// 修改时允许接收的表单中的字段
	'updateFields' => "array('id','agentname','password','phone','retime','domain','qq','wx','money','txname','txfl')",
	'validate' => "
		array('agentname', 'require', '代理名不能为空！', 1, 'regex', 3),
		array('agentname', '1,18', '代理名的值最长不能超过 18 个字符！', 1, 'length', 3),
		array('password', 'require', '密码不能为空！', 1, 'regex', 3),
		array('password', '1,32', '密码的值最长不能超过 32 个字符！', 1, 'length', 3),
		array('phone', 'require', '手机号不能为空！', 1, 'regex', 3),
		array('phone', '1,11', '手机号的值最长不能超过 11 个字符！', 1, 'length', 3),
		array('retime', 'require', '注册时间不能为空！', 1, 'regex', 3),
		array('domain', 'require', '代理注册时所在域名位置不能为空！', 1, 'regex', 3),
		array('domain', '1,50', '代理注册时所在域名位置的值最长不能超过 50 个字符！', 1, 'length', 3),
		array('qq', '1,13', 'qq号的值最长不能超过 13 个字符！', 2, 'length', 3),
		array('wx', '1,50', '微信号的值最长不能超过 50 个字符！', 2, 'length', 3),
		array('money', '1,255', '金额的值最长不能超过 255 个字符！', 2, 'length', 3),
		array('txname', '1,32', '提现名称的值最长不能超过 32 个字符！', 2, 'length', 3),
		array('txfl', '1,30', '提现费率的值最长不能超过 30 个字符！', 2, 'length', 3),
	",
	/********************** 表中每个字段信息的配置 ****************************/
	'fields' => array(
		'agentname' => array(
			'text' => '代理名',
			'type' => 'text',
			'default' => '',
		),
		'password' => array(
			'text' => '密码',
			'type' => 'password',
			'default' => '',
		),
		'phone' => array(
			'text' => '手机号',
			'type' => 'text',
			'default' => '',
		),
		'retime' => array(
			'text' => '注册时间',
			'type' => 'text',
			'default' => '',
		),
		'domain' => array(
			'text' => '代理注册时所在域名位置',
			'type' => 'text',
			'default' => '',
		),
		'qq' => array(
			'text' => 'qq号',
			'type' => 'text',
			'default' => '',
		),
		'wx' => array(
			'text' => '微信号',
			'type' => 'text',
			'default' => '',
		),
		'money' => array(
			'text' => '金额',
			'type' => 'text',
			'default' => '',
		),
		'txname' => array(
			'text' => '提现名称',
			'type' => 'text',
			'default' => '',
		),
		'txfl' => array(
			'text' => '提现费率',
			'type' => 'text',
			'default' => '',
		),
	),
	/**************** 搜索字段的配置 **********************/
	'search' => array(
		array('agentname', 'normal', '', 'like', '代理名'),
		array('phone', 'normal', '', 'like', '手机号'),
		array('domain', 'normal', '', 'like', '代理注册时所在域名位置'),
	),
);