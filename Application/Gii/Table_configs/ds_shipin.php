<?php
return array(
	'tableName' => 'ds_shipin',    // 表名
	'tableCnName' => '',  // 表的中文名
	'moduleName' => 'Admin',  // 代码生成到的模块
	'withPrivilege' => FALSE,  // 是否生成相应权限的数据
	'topPriName' => '',        // 顶级权限的名称
	'digui' => 0,             // 是否无限级（递归）
	'diguiName' => '',        // 递归时用来显示的字段的名字，如cat_name（分类名称）
	'pk' => 'id',    // 表中主键字段名称
	/********************* 要生成的模型文件中的代码 ******************************/
	// 添加时允许接收的表单中的字段
	'insertFields' => "array('money','sj','url','userid','name','zykey','allow','time')",
	// 修改时允许接收的表单中的字段
	'updateFields' => "array('id','money','sj','url','userid','name','zykey','allow','time')",
	'validate' => "
		array('money', 'require', '打赏金额不能为空！', 1, 'regex', 3),
		array('money', '1,255', '打赏金额的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('sj', 'require', '是否开启随机不能为空！', 1, 'regex', 3),
		array('sj', '1,10', '是否开启随机的值最长不能超过 10 个字符！', 1, 'length', 3),
		array('url', 'require', 'url地址不能为空！', 1, 'regex', 3),
		array('url', '1,255', 'url地址的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('userid', 'require', '发布者id不能为空！', 1, 'regex', 3),
		array('userid', '1,255', '发布者id的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('name', 'require', '资源名称不能为空！', 1, 'regex', 3),
		array('name', '1,255', '资源名称的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('zykey', 'require', '资源key不能为空！', 1, 'regex', 3),
		array('zykey', '1,1000', '资源key的值最长不能超过 1000 个字符！', 1, 'length', 3),
		array('allow', '1,4', '允许观看模式,1为单片观看，2位包月观看，1,2位两者皆有的值最长不能超过 4 个字符！', 2, 'length', 3),
		array('time', 'require', '发布时间不能为空！', 1, 'regex', 3),
	",
	/********************** 表中每个字段信息的配置 ****************************/
	'fields' => array(
		'money' => array(
			'text' => '打赏金额',
			'type' => 'text',
			'default' => '',
		),
		'sj' => array(
			'text' => '是否开启随机',
			'type' => 'radio',
			'default' => '',
		),
		'url' => array(
			'text' => 'url地址',
			'type' => 'text',
			'default' => '',
		),
		'userid' => array(
			'text' => '发布者id',
			'type' => 'text',
			'default' => '',
		),
		'name' => array(
			'text' => '资源名称',
			'type' => 'text',
			'default' => '',
		),
		'zykey' => array(
			'text' => '资源key',
			'type' => 'text',
			'default' => '',
		),
		'allow' => array(
			'text' => '允许观看模式,1为单片观看，2位包月观看，1,2位两者皆有',
			'type' => 'radio',
			'default' => '1,2',
		),
		'time' => array(
			'text' => '发布时间',
			'type' => 'text',
			'default' => '',
		),
	),
	/**************** 搜索字段的配置 **********************/
	'search' => array(

		array('userid', 'normal', '', 'like', '发布者id'),
		array('name', 'normal', '', 'like', '资源名称'),
		array('allow', 'normal', '', 'like', '允许观看模式,1为单片观看，2位包月观看，1,2位两者皆有'),

	),
);