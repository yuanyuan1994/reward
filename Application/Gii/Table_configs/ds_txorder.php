<?php
return array(
	'tableName' => 'ds_txorder',    // 表名
	'tableCnName' => '',  // 表的中文名
	'moduleName' => 'Admin',  // 代码生成到的模块
	'withPrivilege' => FALSE,  // 是否生成相应权限的数据
	'topPriName' => '',        // 顶级权限的名称
	'digui' => 0,             // 是否无限级（递归）
	'diguiName' => '',        // 递归时用来显示的字段的名字，如cat_name（分类名称）
	'pk' => 'id',    // 表中主键字段名称
	/********************* 要生成的模型文件中的代码 ******************************/
	// 添加时允许接收的表单中的字段
	'insertFields' => "array('ddh','time','name','txfl','money','zt')",
	// 修改时允许接收的表单中的字段
	'updateFields' => "array('id','ddh','time','name','txfl','money','zt')",
	'validate' => "
		array('ddh', 'require', '订单号不能为空！', 1, 'regex', 3),
		array('ddh', '1,255', '订单号的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('time', 'require', '申请时间不能为空！', 1, 'regex', 3),
		array('name', 'require', '提款人姓名不能为空！', 1, 'regex', 3),
		array('name', '1,30', '提款人姓名的值最长不能超过 30 个字符！', 1, 'length', 3),
		array('txfl', 'require', '提现费率不能为空！', 1, 'regex', 3),
		array('txfl', '1,255', '提现费率的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('money', 'require', '提现金额不能为空！', 1, 'regex', 3),
		array('money', '1,255', '提现金额的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('zt', 'require', '状态不能为空！', 1, 'regex', 3),
		array('zt', '1,20', '状态的值最长不能超过 20 个字符！', 1, 'length', 3),
	",
	/********************** 表中每个字段信息的配置 ****************************/
	'fields' => array(
		'ddh' => array(
			'text' => '订单号',
			'type' => 'text',
			'default' => '',
		),
		'time' => array(
			'text' => '申请时间',
			'type' => 'text',
			'default' => '',
		),
		'name' => array(
			'text' => '提款人姓名',
			'type' => 'text',
			'default' => '',
		),
		'txfl' => array(
			'text' => '提现费率',
			'type' => 'text',
			'default' => '',
		),
		'money' => array(
			'text' => '提现金额',
			'type' => 'text',
			'default' => '',
		),
		'zt' => array(
			'text' => '状态',
			'type' => 'text',
			'default' => '',
		),
	),
	/**************** 搜索字段的配置 **********************/
	'search' => array(
		array('ddh', 'normal', '', 'like', '订单号'),
		array('name', 'normal', '', 'like', '提款人姓名'),
		array('zt', 'normal', '', 'like', '状态'),
	),
);