<?php
return array(
	'tableName' => 'ds_recharge',    // 表名
	'tableCnName' => '',  // 表的中文名
	'moduleName' => 'Admin',  // 代码生成到的模块
	'withPrivilege' => FALSE,  // 是否生成相应权限的数据
	'topPriName' => '',        // 顶级权限的名称
	'digui' => 0,             // 是否无限级（递归）
	'diguiName' => '',        // 递归时用来显示的字段的名字，如cat_name（分类名称）
	'pk' => 'id',    // 表中主键字段名称
	/********************* 要生成的模型文件中的代码 ******************************/
	// 添加时允许接收的表单中的字段
	'insertFields' => "array('ddh','time','userid','zt','money','vipname')",
	// 修改时允许接收的表单中的字段
	'updateFields' => "array('id','ddh','time','userid','zt','money','vipname')",
	'validate' => "
		array('ddh', 'require', '充值订单号不能为空！', 1, 'regex', 3),
		array('ddh', '1,255', '充值订单号的值最长不能超过 255 个字符！', 1, 'length', 3),
		array('time', 'require', '充值时间不能为空！', 1, 'regex', 1),
		array('userid', 'require', '充值用户ID不能为空！', 1, 'regex', 3),
		array('userid', 'number', '充值用户ID必须是一个整数！', 1, 'regex', 3),
		array('zt', 'require', '充值状态不能为空！', 1, 'regex', 3),
		array('zt', '1,25', '充值状态的值最长不能超过 25 个字符！', 1, 'length', 3),
		array('money', 'require', '充值金额不能为空！', 1, 'regex', 3),
		array('vipname', 'require', 'vip名称不能为空！', 1, 'regex', 3),
		array('vipname', '1,255', 'vip名称的值最长不能超过 255 个字符！', 1, 'length', 3),
	",
	/********************** 表中每个字段信息的配置 ****************************/
	'fields' => array(
		'ddh' => array(
			'text' => '充值订单号',
			'type' => 'text',
			'default' => '',
		),
		'time' => array(
			'text' => '充值时间',
			'type' => 'text',
			'default' => '',
		),
		'userid' => array(
			'text' => '充值用户ID',
			'type' => 'text',
			'default' => '',
		),
		'zt' => array(
			'text' => '充值状态',
			'type' => 'text',
			'default' => '',
		),
		'money' => array(
			'text' => '充值金额',
			'type' => 'text',
			'default' => '',
		),
		'vipname' => array(
			'text' => 'vip名称',
			'type' => 'text',
			'default' => '',
		),
	),
	/**************** 搜索字段的配置 **********************/
	'search' => array(
		array('zt', 'normal', '', 'like', '充值状态'),
	),
);