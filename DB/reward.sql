/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : reward

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-05-11 18:07:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ds_admin`
-- ----------------------------
DROP TABLE IF EXISTS `ds_admin`;
CREATE TABLE `ds_admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(18) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ds_admin
-- ----------------------------
INSERT INTO `ds_admin` VALUES ('1000', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- ----------------------------
-- Table structure for `ds_advert`
-- ----------------------------
DROP TABLE IF EXISTS `ds_advert`;
CREATE TABLE `ds_advert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(50) DEFAULT '' COMMENT '活动图片',
  `start_time` int(15) DEFAULT NULL COMMENT '开始时间',
  `end_time` int(15) DEFAULT NULL COMMENT '结束时间',
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ds_advert
-- ----------------------------
INSERT INTO `ds_advert` VALUES ('1', 'image/2018-05-11/5af54db7efa81.png', '1525622400', '1526486400', 'http://www.baidu.com');
INSERT INTO `ds_advert` VALUES ('2', 'image/2018-05-11/5af54db7efa81.png', '1525622400', '1526486400', 'http://www.baidu.com');
INSERT INTO `ds_advert` VALUES ('3', 'image/2018-05-11/5af55b8449392.jpg', '1525881600', '1527177600', '123123');

-- ----------------------------
-- Table structure for `ds_agent`
-- ----------------------------
DROP TABLE IF EXISTS `ds_agent`;
CREATE TABLE `ds_agent` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `agentname` varchar(18) NOT NULL COMMENT '代理名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `phone` char(11) NOT NULL COMMENT '手机号',
  `retime` int(15) NOT NULL COMMENT '注册时间',
  `domain` varchar(255) NOT NULL COMMENT '代理注册时所在域名位置',
  `qq` varchar(13) DEFAULT NULL COMMENT 'qq号',
  `wx` varchar(50) DEFAULT NULL COMMENT '微信号',
  `money` varchar(255) DEFAULT '0' COMMENT '金额',
  `txname` varchar(32) DEFAULT NULL COMMENT '提现名称',
  `txfl` varchar(30) DEFAULT NULL COMMENT '提现费率',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ds_agent
-- ----------------------------
INSERT INTO `ds_agent` VALUES ('1', 'xiaocai', '548c71f18162b32b59af4a5671252bab', '13799164444', '1526005104', 'http://www.xiaomei.com', '1011111111', '1111111111', '1000', '张三', '10');

-- ----------------------------
-- Table structure for `ds_config`
-- ----------------------------
DROP TABLE IF EXISTS `ds_config`;
CREATE TABLE `ds_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `zym` varchar(255) NOT NULL COMMENT '默认主机名',
  `dwz` varchar(255) NOT NULL DEFAULT '1' COMMENT '短网址接口类型',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ds_config
-- ----------------------------
INSERT INTO `ds_config` VALUES ('1', 'http://reward.com', '10');

-- ----------------------------
-- Table structure for `ds_domain`
-- ----------------------------
DROP TABLE IF EXISTS `ds_domain`;
CREATE TABLE `ds_domain` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domainlink` varchar(255) NOT NULL COMMENT '域名',
  `zt` varchar(255) NOT NULL DEFAULT '1' COMMENT '域名状态，1未分配，2已分配',
  `userid` varchar(255) DEFAULT NULL COMMENT '使用者ID',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ds_domain
-- ----------------------------
INSERT INTO `ds_domain` VALUES ('1', 'http://www.xiaotian.com', '2', '1');
INSERT INTO `ds_domain` VALUES ('2', 'http://www.xiaomei.com', '2', '1');
INSERT INTO `ds_domain` VALUES ('4', 'http://www.xiaobing.com', '1', '');
INSERT INTO `ds_domain` VALUES ('5', 'http://www.xiaohua.com', '1', '');
INSERT INTO `ds_domain` VALUES ('6', 'http://xiaodao.com', '2', '3');

-- ----------------------------
-- Table structure for `ds_history`
-- ----------------------------
DROP TABLE IF EXISTS `ds_history`;
CREATE TABLE `ds_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '视频名称',
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL COMMENT '视频地址',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `spid` int(11) NOT NULL COMMENT '视频id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ds_history
-- ----------------------------
INSERT INTO `ds_history` VALUES ('1', '2', 'http://otca055pb.bkt.clouddn.com/38a888piCpKX.mp4', '1', '16');

-- ----------------------------
-- Table structure for `ds_member`
-- ----------------------------
DROP TABLE IF EXISTS `ds_member`;
CREATE TABLE `ds_member` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(18) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `phone` char(11) NOT NULL COMMENT '手机号',
  `retime` int(15) NOT NULL COMMENT '注册时间',
  `vipid` char(4) NOT NULL DEFAULT '0' COMMENT '会员类型ID',
  `viptime` int(15) NOT NULL COMMENT '会员过期时间',
  `domain` varchar(50) NOT NULL COMMENT '会员注册时所在域名位置',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ds_member
-- ----------------------------
INSERT INTO `ds_member` VALUES ('1', 'zhangsan', '01d7f40760960e7bd9443513f22ab9af', '13325825454', '1525837207', '1', '1925837207', 'admin');
INSERT INTO `ds_member` VALUES ('2', 'xiaobai', '21232f297a57a5a743894a0e4a801fc3', '13799162222', '1525848607', '1', '0', '1');
INSERT INTO `ds_member` VALUES ('3', '测试账户', 'e10adc3949ba59abbe56e057f20f883e', '13507474747', '1525848766', '1', '0', 'admin');
INSERT INTO `ds_member` VALUES ('4', 'xiaoxiaosu', '97dd0563a95651bfad57a44730f2482d', '13799999999', '1525848806', '1', '0', '1');
INSERT INTO `ds_member` VALUES ('5', 'aixi', '202cb962ac59075b964b07152d234b70', '13799999999', '1525853214', '1', '1525853214', 'admin');
INSERT INTO `ds_member` VALUES ('6', 'aibai', '202cb962ac59075b964b07152d234b70', '13799999999', '1525853229', '0', '1525853229', 'admin');
INSERT INTO `ds_member` VALUES ('7', '123', '202cb962ac59075b964b07152d234b70', '13799111111', '1526027202', '0', '0', 'admin');
INSERT INTO `ds_member` VALUES ('8', '123123', '4297f44b13955235245b2497399d7a93', '13799111111', '1526027210', '0', '0', 'admin');
INSERT INTO `ds_member` VALUES ('9', '123123321', 'f5bb0c8de146c67b44babbf4e6584cc0', '13799111111', '1526027220', '0', '0', 'admin');
INSERT INTO `ds_member` VALUES ('10', '123123123123123', '9828f915d621b9a0a5666fc21e5ee84f', '13799111111', '1526027230', '0', '0', 'admin');
INSERT INTO `ds_member` VALUES ('11', '123123123', '4297f44b13955235245b2497399d7a93', '13799111111', '1526027236', '0', '0', 'admin');
INSERT INTO `ds_member` VALUES ('12', '1231qweqw', '4297f44b13955235245b2497399d7a93', '13799111111', '1526027245', '0', '0', 'admin');
INSERT INTO `ds_member` VALUES ('13', '123123123asd', 'bfd59291e825b5f2bbf1eb76569f8fe7', '13799111111', '1526027258', '0', '0', 'admin');
INSERT INTO `ds_member` VALUES ('14', '1', 'f3ac63c91272f19ce97c7397825cc15f', '13799111111', '1526027275', '0', '0', 'admin');
INSERT INTO `ds_member` VALUES ('15', 'asasd', 'a8f5f167f44f4964e6c998dee827110c', '13799111111', '1526027281', '0', '0', 'admin');
INSERT INTO `ds_member` VALUES ('16', 'qweqweqwe', 'b26986ceee60f744534aaab928cc12df', '13799111111', '1526027293', '0', '0', 'admin');

-- ----------------------------
-- Table structure for `ds_recharge`
-- ----------------------------
DROP TABLE IF EXISTS `ds_recharge`;
CREATE TABLE `ds_recharge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `ddh` varchar(255) NOT NULL COMMENT '充值订单号',
  `time` int(15) NOT NULL COMMENT '充值时间',
  `userid` int(11) NOT NULL COMMENT '充值用户ID',
  `zt` varchar(25) NOT NULL DEFAULT '1' COMMENT '充值状态',
  `money` float NOT NULL COMMENT '充值金额',
  `vipname` varchar(255) NOT NULL COMMENT 'vip名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ds_recharge
-- ----------------------------

-- ----------------------------
-- Table structure for `ds_shipin`
-- ----------------------------
DROP TABLE IF EXISTS `ds_shipin`;
CREATE TABLE `ds_shipin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `money` varchar(255) NOT NULL COMMENT '打赏金额',
  `sj` varchar(10) NOT NULL DEFAULT '1' COMMENT '是否开启随机',
  `url` varchar(255) NOT NULL COMMENT 'url地址',
  `userid` varchar(255) NOT NULL COMMENT '发布者id',
  `name` varchar(255) NOT NULL COMMENT '资源名称',
  `zykey` varchar(1000) NOT NULL COMMENT '资源key',
  `allow` varchar(4) NOT NULL DEFAULT '3' COMMENT '允许观看模式,1为单片观看，2位包月观看，3位两者皆有',
  `time` int(15) NOT NULL COMMENT '发布时间',
  `sptype` int(11) NOT NULL COMMENT '类型ID',
  `cs` int(11) NOT NULL DEFAULT '0' COMMENT '打赏次数',
  `bigpic` varchar(255) NOT NULL COMMENT '大图',
  `smallpic` varchar(255) NOT NULL COMMENT '小图',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ds_shipin
-- ----------------------------
INSERT INTO `ds_shipin` VALUES ('17', '22', '1', 'http://otca055pb.bkt.clouddn.com/38a888piCpKX.mp4', '1', '2', '8d5e957f297893487bd98fa830fa6413', '1', '1526029524', '1', '0', 'video/2018-05-11/5af5322c0517b.jpg', 'video/2018-05-11/thumb_0_5af5322c0517b.jpg');
INSERT INTO `ds_shipin` VALUES ('16', '22', '1', 'http://otca055pb.bkt.clouddn.com/38a888piCpKX.mp4', 'admin', '2', '8d5e957f297893487bd98fa830fa6413', '1', '1526018604', '1', '0', 'video/2018-05-11/5af5322c0517b.jpg', 'video/2018-05-11/thumb_0_5af5322c0517b.jpg');

-- ----------------------------
-- Table structure for `ds_sptype`
-- ----------------------------
DROP TABLE IF EXISTS `ds_sptype`;
CREATE TABLE `ds_sptype` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT '分类名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ds_sptype
-- ----------------------------
INSERT INTO `ds_sptype` VALUES ('1', '国产片');
INSERT INTO `ds_sptype` VALUES ('3', '日本片');
INSERT INTO `ds_sptype` VALUES ('4', '台湾片');

-- ----------------------------
-- Table structure for `ds_tglink`
-- ----------------------------
DROP TABLE IF EXISTS `ds_tglink`;
CREATE TABLE `ds_tglink` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(50) NOT NULL COMMENT '资源名称',
  `shortlink` varchar(255) NOT NULL COMMENT '短链接',
  `code` varchar(255) NOT NULL COMMENT '资源code',
  `userid` varchar(255) NOT NULL COMMENT '连接拥有者',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ds_tglink
-- ----------------------------
INSERT INTO `ds_tglink` VALUES ('1', '国产片', 'http://t.cn/R3Zjsbr', '0e65972dce68dad4d52d063967f0a705aa28119e96', '1000');
INSERT INTO `ds_tglink` VALUES ('2', '2', 'http://t.cn/R3bsaPX', '8d5e957f297893487bd98fa830fa641394022db78f', '1');

-- ----------------------------
-- Table structure for `ds_txorder`
-- ----------------------------
DROP TABLE IF EXISTS `ds_txorder`;
CREATE TABLE `ds_txorder` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `ddh` varchar(255) NOT NULL COMMENT '订单号',
  `time` int(11) NOT NULL COMMENT '申请时间',
  `name` varchar(30) NOT NULL COMMENT '提款人姓名',
  `txfl` varchar(255) NOT NULL COMMENT '提现费率',
  `money` varchar(255) NOT NULL COMMENT '提现金额',
  `zt` varchar(20) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ds_txorder
-- ----------------------------
INSERT INTO `ds_txorder` VALUES ('1', '15258482737521', '1525848273', 'xiaotian', '1', '2222', '1');
INSERT INTO `ds_txorder` VALUES ('2', '15258484197285', '1525848419', 'xiaotian', '1', '82', '1');
INSERT INTO `ds_txorder` VALUES ('3', '15258484346952', '1525848434', 'xiaotian', '1', '222', '1');
INSERT INTO `ds_txorder` VALUES ('4', '15258484503062', '1525848450', 'xiaotian', '1', '1', '1');

-- ----------------------------
-- Table structure for `ds_vip`
-- ----------------------------
DROP TABLE IF EXISTS `ds_vip`;
CREATE TABLE `ds_vip` (
  `id` tinyint(10) unsigned NOT NULL AUTO_INCREMENT,
  `money` float(10,0) NOT NULL COMMENT '价格',
  `name` varchar(255) NOT NULL COMMENT '会员类型名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ds_vip
-- ----------------------------
INSERT INTO `ds_vip` VALUES ('1', '29', '三个月');
INSERT INTO `ds_vip` VALUES ('2', '10', '一个月');
INSERT INTO `ds_vip` VALUES ('3', '109', '12个月');
